process.env.DISABLE_NOTIFIER = false;
var elixir = require('laravel-elixir');

// var gulp = require("gulp");
// var babel = require("gulp-babel");
//


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
//
elixir(function(mix) {
    mix .sass('app.scss')
        .sass('dashboard.scss')
        .sass('index.scss')
        .sass('register.scss')
        .sass('login.scss')
        .sass('sidebar.scss')
        .sass('profile-edit.scss')
        .sass('profile.scss')
        .sass('staff.scss')
        .sass('payments.scss')
        .sass('workplace.scss')
        .sass('products.scss')
        .sass('ingredients.scss')
        .sass('orders.scss')
        .sass('pages.scss')
        .sass('shifts.scss')
        .sass('settings.scss')
        .sass('tables.scss');
    mix.scripts(

        [
        'orderCharts.js'
    ], 'public/js/orderCharts.js')
        .scripts('general.js', 'public/js/general.js')
        .scripts('dashboard.js', 'public/js/dashboard.js')
        .scripts([
            './vendor/components/jquery/jquery.js'
        ], 'public/js/jQuery.js')
        .scripts(['workplace.js', 'workplace_queries.js','workplace_general.js'],'public/js/workplaceCombined.js')
        .scripts('workplace.js', 'public/js/workplace.js')
        .scripts('workplace_general.js', 'public/js/workplace_general.js')
        .scripts('flashMessages.js', 'public/js/flashMessages.js')
        .scripts('payments.js','public/js/payments.js')
        .scripts('settings.js', 'public/js/settings.js')

    // gulp.task("default", function () {
    //     return gulp.src("resources/assets/js/workplace/*.js")
    //         .pipe(babel({
    //             presets: ['@babel/env']
    //         }))
    //         .pipe(gulp.dest("public/js"));
    // });
});
