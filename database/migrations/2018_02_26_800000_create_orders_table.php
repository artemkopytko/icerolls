<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('quantity')->unsigned()->nullable()->default(1);
            $table->timestamp('date')->nullable()->default(\Carbon\Carbon::now());

	        $table->dropPrimary('id');
            $table->primary(['id','staff_id','product_id','date']);
            $table->foreign('staff_id')
                  ->references('id')
                  ->on('staff');
			$table->foreign('product_id')
			      ->references('id')
			      ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
