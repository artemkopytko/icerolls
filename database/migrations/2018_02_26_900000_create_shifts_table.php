<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->date('date');
            $table->integer('staff_id')->unsigned();
        	$table->increments('id');
	        $table->string('description')->nullable();
	        $table->time('starting_at')->nullable()->default('10:00');
	        $table->time('finishing_at')->nullable()->default('20:00');

	        $table->dropPrimary('id');
	        $table->primary(['date','staff_id']);
	        $table->foreign('staff_id')
	              ->references('id')
	              ->on('staff');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
