<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('payments', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('type');
		    $table->decimal('amount')->unsigned();
		    $table->date('date')->default('now()');
		    $table->date('valid_till')->nullable();
	    });

	    /* FUTURE ONE, USING table FOR TYPES
	    Schema::create('paymenttypes', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('type');
	    });
	    Schema::create('payments', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('type_id');
		    $table->decimal('amount')->unsigned();
		    $table->date('date')->default('now()');
		    $table->date('valid_till')->nullable();
		    $table->foreign('type_id')
		          ->references('id')
		          ->on('paymenttypes')
		          ->onDelete('cascade');
	    });
	    */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('payments');
    }
}
