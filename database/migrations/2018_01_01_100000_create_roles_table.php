<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('roles', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->string('display_name');
	    });

	    DB::table('roles')->insert([
		    'name' => 'admin',
		    'display_name' => 'Администратор'
	    ]);

	    DB::table('roles')->insert([
		    'name' => 'accountant',
		    'display_name' => 'Бухгалтер'
	    ]);

	    DB::table('roles')->insert([
		    'name' => 'staff',
		    'display_name' => 'Стафф'
	    ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
