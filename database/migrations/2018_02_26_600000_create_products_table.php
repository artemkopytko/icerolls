<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('products', function (Blueprint $table) {
		    $table->increments('id');
		    $table->decimal('price')->unsigned();
		    $table->string('title');
		    $table->string('description')->nullable();
		    $table->boolean('actual')->default('true');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
