<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('incomes', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('note');
		    $table->decimal('amount')->unsigned();
		    $table->date('date')->default('now()');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
