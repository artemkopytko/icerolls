<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('ingredients', function (Blueprint $table) {
		    $table->increments('id');
		    $table->decimal('price')->unsigned();
		    $table->string('title');
		    $table->decimal('amountperpack')->unsigned();
		    $table->string('description')->nullable();
	    });

	    Schema::create('product_ingredient', function (Blueprint $table) {
		    $table->integer('product_id')->unsigned();
		    $table->integer('ingredient_id')->unsigned();
		    $table->primary(['product_id', 'ingredient_id']);
		    $table->foreign('product_id')
		          ->references('id')
		          ->on('products')
		          ->onDelete('cascade');
		    $table->foreign('ingredient_id')
		          ->references('id')
		          ->on('ingredients')
		          ->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('product_ingredient');
        Schema::dropIfExists('ingredients');
    }
}
