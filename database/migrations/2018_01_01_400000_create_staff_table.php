<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('staff', function (Blueprint $table) {
		    $table->integer('id');
		    $table->string('name')->nullable();
		    $table->string('surname')->nullable();
		    $table->string('phone')->nullable();
		    $table->decimal('salary')->nullable();
			$table->string('address')->nullable();
			$table->string('position')->nullable();
			$table->primary('id');
			$table->foreign('id')
			      ->references('id')
			      ->on('users')
			      ->onDelete('cascade');
	    });

	    DB::table('staff')->insert([
	    	'id' => 1,
			'position' => 'Администратор'
	    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
