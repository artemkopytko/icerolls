$('.settings-value').on('click', function () {
    console.log('clicked');
    $('#modalCategory').text($(this).data('category'));
    $('label[for="modalField"]').text($(this).data('title'));
    $('#modalField').val($(this).data('value'));
});

$('.settings-form').on('submit', function () {

    event.preventDefault();
    // var id = this.id;


    var editData = {
        category: $('#modalCategory').text(),
        title: $('label[for="modalField"]').text(),
        value: $('#modalField').val()
    };

    console.log(editData);

    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax(
        {
        dataType: 'json',
        type: "POST",
        url: '/settings/editconfig',
        data: JSON.stringify(editData),
        success: function( response ) {

            if(response.status == '200') {
                showSuccessMessage('Успех');
                $('#settingsModal').modal('toggle');
                $('.settings-value').each(function() {
                    if(($(this).data('category') === editData.category) && ($(this).data('title') === editData.title)) {
                        $(this).data('value', editData.value);
                        $(this).text(editData.value);
                    }
                })
            } else {
                showFailMessage('Неудача');
                $('#settingsModal').modal('toggle');
            }

            console.log(response);

            //
            // $(row).parent().css('background-color', 'lightcoral');
            // $(row).parent().fadeOut(1000);
            // setInterval(function(){
            //     redirect('staff');
            // }, 500);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log('error throw');
            console.log(errorThrown);

        }
    });
});
//# sourceMappingURL=settings.js.map
