'use strict';

//FUNCTION USED TO PRINT DATE (with month name) AND CURRENT TIME
var popupVisible = false;
var orderPopupVisible = false;


function printDateAndTime() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let monthString = '';

    switch (mm)
    {
        case 1:
            monthString = 'Января';
            break;
        case 2:
            monthString = 'Февраля';
            break;
        case 3:
            monthString = 'Марта';
            break;
        case 4:
            monthString = 'Апреля';
            break;
        case 5:
            monthString = 'Мая';
            break;
        case 6:
            monthString = 'Июня';
            break;
        case 7:
            monthString = 'Июля';
            break;
        case 8:
            monthString = 'Августа';
            break;
        case 9:
            monthString = 'Сентября';
            break;
        case 10:
            monthString = 'Октября';
            break;
        case 11:
            monthString = 'Ноября';
            break;
        case 12:
            monthString = 'Декабря';
            break;
        default:
            monthString = '?'
    }

    document.getElementById('date').innerHTML = dd + ' ' + monthString;

    (function () {
        function checkTime(i) {
            return (i < 10) ? "0" + i : i;
        }

        function startTime() {
            let today = new Date(),
                h = checkTime(today.getHours()),
                m = checkTime(today.getMinutes());
            document.getElementById('time').innerHTML = h + ":" + m;
            let t = setTimeout(function () {
                startTime()
            }, 500);
        }
        startTime();
    })();
}


function exit(event) {
    event.preventDefault();

    var result = confirm('Уверены, что хотите выйти?');

    if (result) {
        window.location.href = "/dashboard";
    }
}


function resetView() {
    view.update();
}

// jQuery(document).on('click',function (e) {
//     var el = '.popup-form';
//     if (jQuery(e.target).closest(el).length) return;
//     console.log('click outside of popup form');
//     console.log(popupVisible);
//     if(popupVisible) {
//         popupVisible = !popupVisible;
//         $('popup-wrapper').hide();
//     }
// });

function togglePopup(orderPositionItem, id) {
    console.log('Popup toggled!');
    if(popupVisible) {
        //LOCKING EDITING
        $('.workplace-popup-form-item.editable').each(function(index) {
            // $(this).removeClass('unlocked');
            $(this).addClass('locked');
        });

        popupEditingAllowed = false;
        var lockButton = $('#allowEditing');
        showFailMessage('Редактирование запрещено');
        lockButton.removeClass('unlocked');
        lockButton.addClass('locked');
        lockButton.html('Разрешить редактирование <div class="locked-icon"><i class="fas fa-lock"></i></div>\n' +
            '                    <div class="unlocked-icon"><i class="fas fa-unlock"></i></div>');

        $('.workplace-popup-form-item.editable').css('backgroundColor', 'transparent');
        $('.workplace-popup-form-item.editable').css('outline', 'none');
        popupActionValue.text('');
        popupActionName.text('');
        $('.popup-form-action-value-controls').hide();
        $('.popup-wrapper').hide();
    } else {
        // $('.workplace-popup-form-item.editable').css('backgroundColor', 'transparent');
        // $('.workplace-popup-form-item.editable').css('outline', 'none');
        // popupActionValue.text('');
        $('.popup-form-action-value-controls').hide(); // Hiding popup action contol groups
        $('#popup-id').text(id);
        $('.popup-wrapper').show();
        $('#popup-name').text(orderPositionItem.name);
        $('#popup-price').text(orderPositionItem.price);
        $('#popup-quantity').text(orderPositionItem.quantity);
        $('#popup-quantity').attr("data-price", orderPositionItem.price);
        $("#popup-quantity").attr("onchange","quantityEdited("+id+");");
        var price = orderPositionItem.quantity * orderPositionItem.price - (orderPositionItem.quantity * orderPositionItem.price * orderPositionItem.discount / 100);
        $('#popup-sum').text(price);

        $('#popup-delete').attr("onclick","deleteOrderPosition("+id+");");
    }
    popupVisible = !popupVisible;
}

function toggleOrderPopup() {

    if(!orderPopupVisible) {

        console.log('ORDER POPUP OPENED');
        $('.popup-order-wrapper').show();

    } else {

        console.log('ORDER POPUP CLOSED');
        $('.popup-order-wrapper').hide();

    }
    orderPopupVisible = !orderPopupVisible;
}

$('#popup-order-close').on('click', function () {
  toggleOrderPopup();
});

// $('.order-total-row').on('click', function () {
//
// });

printDateAndTime();
//# sourceMappingURL=workplace_general.js.map
