'use strict';


// Ajax calls

/*
* NAVIGATION
*
* 1) Fetch categories from the database
* 2) Fetch all products from the database
* 3) Submit an order
*
* */

// Fetch categories from the database
function fetchCategories() {
    // var id = this.id;
    let token = $('meta[name=token]').attr("content");
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/products/getCategories',
        async: false,
        data: {_token: token},
        success: function( response ) {
            products.categories = (response.categories);
            console.log(response.categories);
        }
    });
}

// Fetch all products from the database

function fetchProducts() {
    // var id = this.id;
    let token = $('meta[name=token]').attr("content");
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/products/getProducts',
        async: false,
        data: {_token: token},
        success: function( response ) {
            console.log(response.products);
            products.products = (response.products);
        }
    });
}

// Submit an order

function submitOrder() {
    console.log(JSON.stringify(order.positions));
    console.log('SENDING ORDER ',order.positions, ' to the database');

    if(order.temporaryPositions)
        console.log('ALSO TEMPORARY: ', order.temporaryPositions);

    const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = [];
    data.push(
        {
            order: order.positions,
            _method: 'POST',
            _token: CSRF_TOKEN
        }
        );

    data.push(
        {
            order: order.temporaryPositions,
            _method: 'POST',
            _token: CSRF_TOKEN
        }
    );

    // data.order = order.positions;

    // data.order = order.positions.concat(order.temporaryPositions);
    // data._method = 'POST';
    // data._token = CSRF_TOKEN;

    console.log('sending order', data);

    $.ajax({
        // headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        // },
        type: "POST",
        url: '/orders/api/store/',
        data: JSON.stringify(data),
        success: function( response ) {
            var htmlString = '';
            var messagesPopup =  $('#messages');
            if(response.status === '200') {
                console.log('Success: ', response);
                showSuccessMessage(response.message);
                statusIndicator.addClass('status-indicator-ok');
                statusIndicator.removeClass('status-indicator-error');
                // htmlString = '<div class=success>' +  + '</div>';
                // messagesPopup.html(htmlString);

                order.temporaryPositions = [];

                order.clear();
                view.updateOrder();
            } else {
                // console.log('Error: ', response);
                // showFailMessage(response.message);
                // statusIndicator.removeClass('status-indicator-ok');
                // statusIndicator.addClass('status-indicator-error');
                // htmlString = '<div class=fail>' +  + '</div>';
                // messagesPopup.html(htmlString);

                // console.log(response.error);
                // response.error.forEach(function(item){
                //     order.temporaryPositions.push(item)
                // });
                // view.updateOrder();
                // console.log(order.temporaryPositions);
                // order.temporaryPositions.push()


                console.log('error throw');
                order.temporaryPositions = [];

                data.forEach(function(element) {
                    order.temporaryPositions.push(element.order);
                });



                showFailMessage('Заказ не был отправлен');
                statusIndicator.removeClass('status-indicator-ok');
                statusIndicator.addClass('status-indicator-error');
                // htmlString = '<div class=fail>' +  + '</div>';
                // messagesPopup.html(htmlString);

                order.temporaryPositions = order.temporaryPositions.concat(data.order);
                console.log('TEMPORARY IS BIGGER: ', order.temporaryPositions);
                order.clear();
                view.updateOrder();
            }
            // messagesPopup.show();
            // setTimeout(function () {
            //     messagesPopup.hide();
            // }, 3000);
            // console.log(response);
            getTodayOrders();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log('error throw');
            console.log(errorThrown);
            // order.temporaryPositions = [];


            showFailMessage('Заказ не был отправлен');
            statusIndicator.removeClass('status-indicator-ok');
            statusIndicator.addClass('status-indicator-error');
            // htmlString = '<div class=fail>' +  + '</div>';
            // messagesPopup.html(htmlString);

            order.temporaryPositions = order.temporaryPositions.concat(data.order);
            console.log('TEMPORARY IS BIGGER: ', order.temporaryPositions);
            order.clear();
            view.updateOrder();

        }
    });
}



// VARIABLES

let currentCategory = '';
let currentProductID = 0;
let currentPortionNumber = 0;
let currentProduct = new Object();
let selectedPopupAction;
let popupEditingAllowed = false;
let popupQuantityValue = 0;
let popupDiscountValue = 0;
let orderPopupDiscountEntire = 0;

let orderOutputField = document.getElementById('check-content');
let categoriesOutputField = document.getElementById('categories-content');
let productsOutputField = document.getElementById('products-content');
let orderTotalField = document.getElementById('order-total');
let orderPopupPositions= document.getElementById('order-popup-positions');
let currentProductField = document.getElementById('order-content');
let orderPopupTotal = document.getElementById('order-popup-total');
let portionsNumber = $('#product-portions');
let orderControl = $('.order-controls');
let submitButton = $('#order-submit');
let popupActionName = $('#popup-form-controls-action');
// let messagesPopup = $('#messages');
let popupActionValue = $('#popup-form-controls-value');
let popupActionButtonQuantityPlus = $('#popup-form-action-quantity-plus');
let popupActionButtonQuantityMinus = $('#popup-form-action-quantity-minus');
let statusIndicator = $('#status-indicator');

let order = new Order();
let view = new View();
let products = new Products();

//
function View() {

    this.update = function () {
        //RESET CURRENT OUTPUT
        orderOutputField.innerHTML = '';
        categoriesOutputField.innerHTML = '';
        productsOutputField.innerHTML = '';
        currentProductField.innerHTML = '';
        portionsNumber.hide();
        orderControl.hide();

        order.getAll('write');
        order.printOrderTotal();

        products.printCategories();

        order.isEmpty() ? submitButton.hide() : submitButton.show();

        // setOrderPositionHandlers();

        // resetHandlers();
    };

    this.updateOrder = function () {
        orderOutputField.innerHTML = '';
        order.getAll('write');
        order.isEmpty() ? submitButton.hide() : submitButton.show();
        order.printOrderTotal();
    };

    this.updateProductsList = function () {
        //RESET CURRENT PRODUCT OUTPUT
        productsOutputField.innerHTML = '';

        if(currentCategory !== '') {
            products.printCategoryProducts(currentCategory);
        }
    };

    this.clearCurrentProduct = function () {
        currentProductField.innerHTML = null;
        portionsNumber.hide();
    };

    this.printCurrentProduct = function (id) {
        for(let i = 0; i < products.products.length; i += 1) {
            if(products.products[i].id == id) {
                //НАШЛИ, ЗАПИСАЛИ В ПЕРЕМЕННУЮ
                currentProduct = products.products[i];
            }
        }

        this.clearCurrentProduct();

        //ВЫВОДИМ ИНФОРМАЦИЮ
        // currentProductField.innerHTML += '<p>'+currentProduct.id+'</p>';
        currentProductField.innerHTML += '<p>Позиция: <b>'+currentProduct.title+'</b></p>';
        currentProductField.innerHTML += '<p>Описание: '+currentProduct.description+'</p>';
        currentProductField.innerHTML += '<p>Стоимость: '+currentProduct.price+'</p>';
        currentProductField.innerHTML += '<p>Категория: '+currentProduct.category+'</p>';

        portionsNumber.show();
    }
}

function Order() {

    this.positions = [];
    this.temporaryPositions = [];

    //ADD A PRODUCT OT AN ORDER
    this.addPosition = function (position) {
        this.positions.push(position)
    };

    //PRINT PRODUCTS LIST TO A CONSOLE
    // 2 OPTIONS:
    // TO CONSOLE
    // PRINT TO THE ORDER COLUMN
    this.getAll = function (method) {
        if(method === 'console') {
            return console.log(this.positions);
        } else if (method === 'write') {
            orderOutputField.innerHTML = '';
            orderPopupPositions.innerHTML = '';

            orderOutputField.innerHTML += ' <div class="row">\n' +
                '                <div class="cell-small">#</div>\n' +
                '                <div class="cell-big">Название</div>\n' +
                '                <div class="cell-small">Кол-во</div>\n' +
                '                <div class="cell">Сумма</div>\n' +
                '            </div>';

            for(let i = 0; i < this.positions.length; i += 1) {
                let price = (this.positions[i].quantity * this.positions[i].price) - (this.positions[i].quantity * this.positions[i].price * this.positions[i].discount / 100);

                console.log(price);
                let formattedRow = '<div class="row" data-id='+i+'>' +
                    '<div class="cell-small">' + i + '</div>' +
                    '<div class="cell-big">' + this.positions[i].name + '</div>' +
                    '<div class="cell-small">' + this.positions[i].quantity + '</div>' +
                    '<div class="cell">' + price + '</div>' +
                    '</div>';

                orderOutputField.innerHTML += formattedRow;

                let formattedPopupRow = '<div class="popup-row" data-id='+i+'>' +
                    '<div class="cell-small">' + i + '</div>' +
                    '<div class="cell-big">' + this.positions[i].name + '</div>' +
                    '<div class="cell-small">' + this.positions[i].quantity + '</div>' +
                    '<div class="cell">' + price + '</div>' +
                    '</div>';

                orderPopupPositions.innerHTML += formattedPopupRow;
            }
            orderPositionHandler();

        } else {
            return console.log('Error fetching order');
        }
    };

    this.isEmpty = function () {
        return (this.positions.length === 0);
    };


    this.printOrderTotal = function () {

        let total = 0;

        for(let i = 0; i < this.positions.length; i += 1) {
                total += (this.positions[i].quantity * this.positions[i].price) - (this.positions[i].quantity * this.positions[i].price * this.positions[i].discount / 100);;
        //    TODO: DISCOUNT
        }

        orderTotalField.innerHTML = '₴ ';
        orderTotalField.innerHTML += total;

        orderPopupTotal.innerHTML = 'К оплате: ₴';
        orderPopupTotal.innerHTML += total;

        // console.log(total)
    };

    this.clear = function () {
        this.positions = [];
    }
}


function Products() {

    this.categories = new Array();
    this.products = new Array();

    this.printCategories = function () {

        for(let i = 0; i < this.categories.length; i += 1) {
            let formatedRow = '<div class="category">' +
                '<button data-category-name="'+this.categories[i].category+'"' +
                ' class="category-button">' +
                this.categories[i].category +
                '</button>';
            categoriesOutputField.innerHTML += formatedRow;
        }

        updateHandlers();

    };


    // Display products which belong to selected category

    this.printCategoryProducts = function (name) {
    //    AJAX CALL TO GET ALL CATEGORY PRODUCTS (BY CATEGORY NAME)

        for(let i = 0; i < this.products.length; i += 1) {
            if(this.products[i].category === currentCategory) {
                let formatedRow = '<div class="product">' +
                    '<button data-product-id="' + this.products[i].id + '"' +
                    ' class="product-button">' +
                    this.products[i].title +
                    '</button>';
                productsOutputField.innerHTML += formatedRow;
            }
        }
        updateHandlers();

    };
}

function updateHandlers() {

    // orderPositionHandler();

    $('.category-button').on('click', function () {
            selectCategory($(this));
    });

    $('.product-button').on('click', function () {
            selectProduct($(this));
    });

}

// Toggle Popup window with a selected position from on order
// When clicked on a row with position



function orderPositionHandler() {
    console.log('registred');
    $('.row').on('click', function () {
        var id = $(this).data('id');
        let orderPositionItem = order.positions[id];
        console.log('SENDING TO FORM:', orderPositionItem);
        togglePopup(orderPositionItem, id);
    });
    // $('.check-content .row').on('click', function () {
    // console.log(id);
        // if(id) {
        //     console.log('row click');

            // let orderPositionId = $(this).data('order-position-id');
            // console.log('order ID:', id);
            // console.log(id);
            // console.log(order.positions);
            // console.log('HERE: ',order.positions[id]);
            // for(let i = 0; i < order.positions.length; i += 1) {
            //     console.log(order.positions[i]);
            //     if(order.positions[i].id === id) {
            //         orderPositionItem = order.positions[i];
            //     }
            // }
            // console.log(orderPositionItem);
        // }
    // })
}

//POPUP Controls

function setPopupControlsHandlers() {
    $('.workplace-popup-form-item.editable').on('click', function () {
        if(popupEditingAllowed) {
            $('.popup-form-action-value-controls').hide();
            $('.popup-form-discount-button').css('backgroundColor', 'transparent');
            $('.popup-form-discount-button').css('outline', 'none');
            popupDiscountValue = 0;
            popupActionValue.text('');
            selectedPopupAction = $(this).data('action');
            console.log('Chosen Action:', selectedPopupAction);
            $('.workplace-popup-form-item.editable').css('backgroundColor', 'transparent');
            $('.workplace-popup-form-item.editable').css('outline', 'none');
            $(this).css('backgroundColor', 'lightblue');
            $(this).css('outline', '2px solid red');
            popupActionName.text('Редактирование '+selectedPopupAction);
            switch (selectedPopupAction) {
                case 'количества порций': {
                    popupActionValue.text('Количество порций: ' + $('#popup-quantity').text());
                    popupQuantityValue = parseInt($('#popup-quantity').text());
                    $('#popup-form-quantity-controls').show();
                    break;
                }
                case 'суммы': {
                    popupActionValue.text('Скидка на позицию: ' + $('#popup-name').text());
                    popupDiscountValue = order.positions[parseInt($('#popup-id').text())].discount;
                    popupQuantityValue = parseInt($('#popup-quantity').text());
                    $('.popup-form-action-value-controls').show();
                    $('#popup-form-quantity-controls').hide();
                    $('#popup-form-discount-controls').show();
                    break;
                }
            }
        }
    })
}

function setDiscountButtonsHandler() {
    $('.popup-form-discount-button').on('click', function () {
        $('.popup-form-discount-button').css('backgroundColor', 'transparent');
        $('.popup-form-discount-button').css('outline', 'none');
        $(this).css('backgroundColor', 'lightblue');
        $(this).css('outline', '2px solid red');
        popupDiscountValue = $(this).data('discount');
        popupActionValue.text('Скидка ' + popupDiscountValue +'% на позицию: ' + $('#popup-name').text());
        order.positions[parseInt($('#popup-id').text())].discount = popupDiscountValue;
        view.updateOrder();
    })
}

function setEditableHandler() {
    $('#allowEditing').on('click', function () {
        popupEditingAllowed = !popupEditingAllowed;
        if(popupEditingAllowed) {
            $('.workplace-popup-form-item.editable').each(function(index) {
               $(this).removeClass('locked');
               // $(this).addClass('unlocked');
            });
            showSuccessMessage('Редактирование разрешено');
            $(this).removeClass('locked');
            $(this).addClass('unlocked');
            $(this).html('Запретить редактирование <div class="locked-icon"><i class="fas fa-lock"></i></div>\n' +
                '                    <div class="unlocked-icon"><i class="fas fa-unlock"></i></div>');
        } else {
            $('.workplace-popup-form-item.editable').each(function(index) {
                // $(this).removeClass('unlocked');
                $(this).addClass('locked');
            });
            showFailMessage('Редактирование запрещено');
            $(this).removeClass('unlocked');
            $(this).addClass('locked');
            $(this).html('Разрешить редактирование <div class="locked-icon"><i class="fas fa-lock"></i></div>\n' +
                '                    <div class="unlocked-icon"><i class="fas fa-unlock"></i></div>');
        }
    })
}


function setPopupActionQuantityEditorsHandlers() {
    popupActionButtonQuantityPlus.on('click', function () {
        popupQuantityValue += 1;
        order.positions[parseInt($('#popup-id').text())].quantity = popupQuantityValue;
        $('#popup-quantity').text(popupQuantityValue);
        $('#popup-sum').text(parseInt(popupQuantityValue * $('#popup-quantity').data('price')));
        popupActionValue.text('Количество порций: ' + popupQuantityValue);

        view.updateOrder();
    });

    popupActionButtonQuantityMinus.on('click', function () {
        console.log(popupQuantityValue);
        popupQuantityValue -= 1;
        if(popupQuantityValue <= 0) {
            order.positions.splice([parseInt($('#popup-id').text())],1);
            showSuccessMessage($('#popup-name').text() + ' удален');

            //CANCELING ACTION CHOICE
            togglePopup();
        } else {
            $('#popup-quantity').text(popupQuantityValue);
            popupActionValue.text('Количество порций: ' + popupQuantityValue);
            // var sumPrice = popupQuantityValue * $('#popup-quantity').data('price') - (popupQuantityValue * $('#popup-quantity').data('price') * )
            $('#popup-sum').text(parseInt(popupQuantityValue * $('#popup-quantity').data('price')));
        }
        view.updateOrder();
    });
}

// function showSuccessMessage(text) {
//     var random = Math.floor(Math.random() * 10000000000000);
//     let htmlString = '<div class="success '+random+'">'+text+'</div>';
//     messagesPopup.append(htmlString);
//     messagesPopup.show();
//     setTimeout(function () {
//         // messagesPopup.hide();
//         $('.'+random).hide('slow',function(){
//             $('.'+random).remove();
//         });
//         // messagesPopup.remove('.'+random);
//     }, 2000);
// }
//
// function showFailMessage(text) {
//     var random = Math.floor(Math.random() * 10000000000000);
//     let htmlString = '<div class="fail '+random+'">'+text+'</div>';
//     messagesPopup.append(htmlString);
//     messagesPopup.show();
//     setTimeout(function () {
//         // messagesPopup.hide();
//         $('.'+random).hide('slow',function(){
//             $('.'+random).remove();
//         });
//         // messagesPopup.remove('.'+random);
//     }, 2000);
// }

// Unlock quantity editing

function allowQuantityEdit() {
    $('#popup-quantity').prop('disabled', false);
    $('#popup-edit').html('✓');
    $("#popup-edit").attr("onclick","disallowQuantityEdit();");
}

function quantityEdited(id) {
    //TODO : EDIT POPUP BUTTON
    // let price = $('#popup-quantity').data('price');
    // order.positions[id].quantity = parseInt($(this).val());
    // $('#popup-sum').val(parseInt(price) * parseInt($(this).val()));
    // view.updateOrder();
}

// Lock quantity editing

function disallowQuantityEdit() {
    $('#popup-quantity').prop('disabled', true);
    $('#popup-edit').html('Редактировать');
    $("#popup-edit").attr("onclick","allowQuantityEdit();");
}

// Delete position from an order on a frontend

function deleteOrderPosition(id) {
    // order.positions[id];
    order.positions.splice(id, 1);
    // for( var i = 0; i < order.positions.length-1; i++){
    //     if ( order.positions[i] === id) {
    //
    //         console.log('REMOVED ITEM');
    //     }
    // }
console.log(order.positions);
    togglePopup();
    view.updateOrder();
}

// function setCategoryHandlers() {
//     $('.category-button').on('click', function () {
//
//     });
// }

// Select Category to be displayed


function selectCategory(elem) {
    currentCategory = elem.data('category-name');
    console.log(currentCategory);
    $('.category-button').css('backgroundColor','lightblue');
    $('.category-button').css('outline','none');
    elem.css('backgroundColor','white');
    elem.css('outline','2px solid red');
    console.log('Chosen category: ', currentCategory);
    view.updateProductsList();
    view.clearCurrentProduct();
}

// function setProductsHandlers() {

// }

// Select product to be added

function selectProduct(elem) {
    currentProductID = elem.attr('data-product-id');
    console.log('Chosen Product ID: ', currentProductID);
    $('.product-button').css('backgroundColor','transparent');
    $('.product-button').css('outline','none');
    elem.css('backgroundColor','lightblue');
    elem.css('outline','2px solid red');

    view.printCurrentProduct(currentProductID);
}

// function setPortionsNumbers() {
//     $('.portion-number-button').on('click', function () {
//
//     });
// }

// Select number of a potions

function setPortionNumber(elem) {
    currentPortionNumber = elem.text();
    console.log('Chosen Number: ', currentPortionNumber);
    $('.portion-number-button').css('backgroundColor','transparent');
    $('.portion-number-button').css('outline','none');
    elem.css('backgroundColor','lightblue');
    elem.css('outline','2px solid red');

    orderControl.show();
}

$('.order-total-row').on('click', function () {
    toggleOrderPopup();
});

// function setOrderAddHandler() {
//     $('#order-add').on('click', function () {
//
//     });
// }

// Add position to an order on a frontend

function orderAdd() {
    let exists = false;
    console.log('Adding: ', currentProduct);

    for(let i = 0; i < order.positions.length; i += 1) {
        if(order.positions[i].name === currentProduct.title) {
            console.log('Adding to existing');
            // console.log(typeof order.positions[i].quantity);
            // console.log( typeof currentProduct.quantity);
            order.positions[i].quantity = parseInt(order.positions[i].quantity);
            order.positions[i].quantity += parseInt(currentPortionNumber);
            exists = true;
        }
    }

    if(!exists) {
        order.addPosition({
            'product_id' : currentProduct.id,
            'name': currentProduct.title,
            'price': currentProduct.price,
            'quantity': currentPortionNumber,
            'discount': 0,
        });
    }

    view.update();

    currentPortionNumber = 0;
    currentProduct = null;

    $('.portion-number-button').css('backgroundColor','transparent');
    $('.portion-number-button').css('outline','none');
}


submitButton.on('click', function () {
    submitOrder();
});

$('#order-popup-clear').on('click', function () {
    let sure = confirm('Вы уверены?');
    if(sure) {
        order.positions = [];
        view.update();
        toggleOrderPopup();
    }
});

$('#order-popup-discount').on('click', function () {
    $('.order-popup-discount-buttons').show();
});

$('.order-popup-form-discount-button').on('click', function () {
    $('.order-popup-form-discount-button').css('backgroundColor', 'transparent');
    $('.order-popup-form-discount-button').css('outline', 'none');
    $(this).css('backgroundColor', 'lightblue');
    $(this).css('outline', '2px solid red');
    orderPopupDiscountEntire = $(this).data('discount');
    // popupActionValue.text('Скидка ' + popupDiscountValue +'% на позицию: ' + $('#popup-name').text());

    console.log(order.positions);

    order.positions.forEach(function(element) {
       element.discount = orderPopupDiscountEntire;
    });
    // order.positions[parseInt($('#popup-id').text())].discount = popupDiscountValue;
    view.updateOrder();
})
// function setOrderCancelHandler() {
//     $('#order-cancel-product').on('click', function () {
//
//
//     });
// }

// Remove focus on selected product
function orderCancel() {
    console.log('Removed: ', currentProduct);

    view.clearCurrentProduct();

    $('.product-button').css('backgroundColor','transparent');
    $('.product-button').css('outline','none');

    currentPortionNumber = 0;
    currentProduct = null;

    portionsNumber.hide();
    orderControl.hide();
}

fetchCategories();
fetchProducts();
getTodayOrders();

$(document).ready(function () {


    console.log('Page Loaded! Categories: ');
    console.log(products.categories);

    console.log('\nProducts: ');
    console.log(products.products);

    $('.popup-wrapper').hide();
    $('.popup-order-wrapper').hide();
    $('.popup-form-action-value-controls').hide(); // Hiding popup action contol groups
    $('.order-popup-discount-buttons').hide();

    console.log('Handlers for portions & order Add');
    // setPortionsNumbers();
    // setOrderAddHandler();
    // setOrderCancelHandler();
    $('.portion-number-button').on('click', function () {
        setPortionNumber($(this));
    });

    console.log('Adding Handler for popup actions');
    setPopupControlsHandlers();
    setEditableHandler();
    setPopupActionQuantityEditorsHandlers();
    setDiscountButtonsHandler();

    console.log('Позиций в заказе: ', order.positions.length);
    console.log('Пустой ли заказ? ', order.isEmpty());



    view.update();
});




//# sourceMappingURL=workplace.js.map
