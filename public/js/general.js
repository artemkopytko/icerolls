'use strict';

let messagesPopup = $('#messages');
let currentUser = $('#current-user');
let todayOrders = $('#today-orders');

function getTodayOrders() {
    var id = currentUser.data('user-id');
    let token = $('meta[name=token]').attr("content");
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "post",
        url: '/orders/getTodayOrdersForStaff/'+id,
        async: false,
        data: {_token: token},
        success: function( response ) {
            console.log(response);
            todayOrders.text('Заказов: '+response.quantity[0].orders);
        }
    });
}

function showSuccessMessage(text) {
    var random = Math.floor(Math.random() * 10000000000000);
    let htmlString = '<div class="alert-success message-alert '+random+'" role="alert">'+text+'</div>';
    messagesPopup.append(htmlString);
    messagesPopup.show();
    setTimeout(function () {
        // messagesPopup.hide();
        $('.'+random).hide('slow',function(){
            $('.'+random).remove();
        });
        // messagesPopup.remove('.'+random);
    }, 2000);
}

function showFailMessage(text) {
    var random = Math.floor(Math.random() * 10000000000000);
    let htmlString = '<div class="message-alert  alert-error '+random+'" role="alert">'+text+'</div>';
    messagesPopup.append(htmlString);
    messagesPopup.show();
    setTimeout(function () {
        // messagesPopup.hide();
        $('.'+random).hide('slow',function(){
            $('.'+random).remove();
        });
        // messagesPopup.remove('.'+random);
    }, 2000);
}
//# sourceMappingURL=general.js.map
