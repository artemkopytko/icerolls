<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="/js/jQuery.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    <script src="/js/flashMessages.js"></script>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<div class="wrapper">

    <div id="messages">

        @if($flash = session('messageError'))
            <div class="alert-error message-alert" role="alert">
                {{ $flash }}
            </div>
        @endif

        @if($flash = session('messageSuccess'))
            <div class="alert-success message-alert" role="alert">
                {{ $flash }}
            </div>
        @endif

        @include('layouts.errors')
    </div>


    <div class="content">
        @include('layouts.nav')
        @yield('content')
    </div>

    @include('layouts.footer')
</div>

</body>
</html>