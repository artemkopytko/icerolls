<header class="header">
    <div class="container">
        <div class="header-container">
            <div class="header-toggle" onclick="toggleSidebar()">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="header-info">
                <div class="user-info">
                    @if($balance[0]->val < 0)
                        <b style="margin-right: 15px; color: darkred;">{{ $balance[0]->val }} </b>
                    @else
                        <b style="margin-right: 15px; color: lightgreen;">{{ $balance[0]->val }} </b>
                    @endif
                    <span>
                        {{ Auth::user()->staff->name != null ? Auth::user()->staff->name : Auth::user()->email  }}
                    </span>

                    <i class="far fa-user-circle"></i>
                    <ul class="user-links-list">
                        <li class="user-links-list-item">
                            <a class="user-link" href="/profile">
                                <i class="fas fa-cog"></i>
                                <span>Настройки</span>
                            </a></li>
                        <li class="user-links-list-item">
                            <a class="user-link" href="/logout">
                                <i class="fas fa-sign-out-alt"></i>
                                <span>Выйти</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>