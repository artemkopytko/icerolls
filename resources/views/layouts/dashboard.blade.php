<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/dashboard.css">
    <link rel="stylesheet" href="/css/sidebar.css">

    <script src="/js/jQuery.js"></script>
    <script src="/js/dashboard.js"></script>
    <script src="/js/flashMessages.js"></script>
</head>
<body>
<div class="wrapper">

    <div id="messages">
        @if($flash = session('messageError'))
            <div class="alert-error message-alert" role="alert">
                {{ $flash }}
            </div>
        @endif

        @if($flash = session('messageSuccess'))
            <div class="alert-success message-alert" role="alert">
                {{ $flash }}
            </div>
        @endif

        @include('layouts.errors')
    </div>

    @include('layouts.db-header')
    @include('layouts.sidebar')

    <main class="content">
        @yield('content')
    </main>
</div>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</body>
</html>