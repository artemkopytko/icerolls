<nav class="navigation">
    <div class="container">
        <ul class="nav-list">
            <li class="nav-item">
                <a class="nav-link" href="#">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Кто Мы?</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Где Нас Найти?</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Мероприятия</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Еще</a>
            </li>
        </ul>
    </div>
</nav>