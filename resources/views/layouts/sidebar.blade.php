<?php
use App\Role;
$role = new Role();
?>

<div id="sidebar" class="sidebar">
    <div class="sidebar-heading">
        <div class="container">
            <div class="sidebar-content">
                <div class="sb-heading-left">
                    <a href="/dashboard">
                        <img src="/images/logo_compressed.png" alt="#">
                    </a>
                    <span>{{ Auth::user()->role->display_name }}</span>
                </div>
               <div class="sb-heading-right">
                   <div class="sidebar-toggle" onclick="toggleSidebar()">
                   </div>
               </div>
            </div>
        </div>
    </div>
    <div class="sidebar-menu">
        <ul class="sb-list">
            <li class="sb-list-item">
                <div class="sb-icon lightBlue">
                    <i class="fas fa-home"></i>
                </div>
                <a class="sb-link" href="/dashboard">Дэшборд</a>
            </li>
            <li class="sb-list-item">
                <div class="sb-icon brown">
                    <i class="fas fa-chart-line"></i>
                </div>
                @if($role->isStaff())
                    <a class="sb-link" href="/orders?staff_id={{auth()->user()->id}}">Заказы</a>
                @else
                    <a class="sb-link" href="/orders">Заказы</a>
                @endif
            </li>
            @if(!$role->isStaff()) {{--RESTICTED FOR STAFF--}}
            <li class="sb-list-item">
                <div class="sb-icon lightBlue">
                    <i class="far fa-credit-card"></i>
                </div>
                <a class="sb-link" href="/payments">Платежи</a>
            </li>
            @endif
            @if(!$role->isStaff()) {{--RESTICTED FOR STAFF--}}
            <li class="sb-list-item">
                <div class="sb-icon black">
                    <i class="fas fa-coins"></i>
                </div>
                <a class="sb-link" href="/incomes">Доходы</a>
            </li>
            @endif
            <li class="sb-list-item">
                <div class="sb-icon lightRed">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <a class="sb-link" href="/shifts">График</a>
            </li>
            <li class="sb-list-item">
                <div class="sb-icon purple">
                    <i class="fas fa-users"></i>
                </div>
                <a class="sb-link" href="/staff">Сотрудники</a>
            </li>
            <li class="sb-list-item">
                <div class="sb-icon blue">
                    <i class="fas fa-ice-cream"></i>
                    {{--<i class="fas fa-utensils"></i>--}}
                </div>
                <a class="sb-link" href="/products">Блюда</a>
            </li>
            <li class="sb-list-item">
                <div class="sb-icon orange">
                    {{--<i class="fas fa-lemon"></i>--}}
                    <i class="fas fa-seedling"></i>
                </div>
                <a class="sb-link" href="/ingredients">Ингредиенты</a>
            </li>

            <li class="sb-list-item">
                <div class="sb-icon black">
                    {{--<i class="fas fa-lemon"></i>--}}
                    <i class="fas fa-briefcase"></i>
                </div>
                <a class="sb-link" href="/workplace">Рабочий стол</a>
            </li>
            @if(!$role->isStaff()) {{--RESTICTED FOR STAFF--}}
            <li class="sb-list-item">
                <div class="sb-icon black">
                    <i class="fas fa-cog"></i>
                </div>
                <a class="sb-link" href="/settings">Настройки</a>
            </li>
            @endif
            {{--@if(!$role->isStaff())--}}
            {{--<li class="sb-list-item">--}}
                {{--<div class="sb-icon orange">--}}
                    {{--<i class="fas fa-table"></i>--}}
                {{--</div>--}}
                {{--<a class="sb-link" href="/tables">Таблицы</a>--}}
            {{--</li>--}}
            {{--@endif--}}
            {{--<li class="sb-list-item">--}}
                {{--<div class="sb-icon lightRed">--}}
                    {{--<i class="fas fa-file"></i>--}}
                {{--</div>--}}
                {{--<a class="sb-link" href="/pages">Страницы</a>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>