@extends('layouts.master')

@section('content')
    <link rel="stylesheet" href="css/index.css">

    <section id="header">
        <div class="container">
            <h1>Header here</h1>
        </div>
    </section>
    <section id="story">
        <div class="container">
            <h2>Some words about our creation</h2>
        </div>
    </section>
    <section id="location">
        <div class="container">
            <h2>Here is a map with out location.</h2>
        </div>
    </section>
    <section id="location">
        <div class="container">
            <h2>Form for contacting us.</h2>
        </div>
    </section>

@endsection