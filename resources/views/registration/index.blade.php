@extends('layouts.master')

@section('content')

    <link rel="stylesheet" href="css/register.css">
    <main class="register">
        <div class="container">
            <div class="signup-form">
                <h1 class="main-heading">Регистрация</h1>
                <form action="/register" method="post">
                    {{ csrf_field() }}
                    <div class="input-block">
                        <input class="input-field" id="name" name="name" type="text"
                               title="Name" placeholder="Имя" required>
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="surname" name="surname" type="text"
                               title="Surname" placeholder="Фамилия" required>
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="phone" name="phone" type="tel"
                               title="Phone" placeholder="Номер Телефона" required>
                        <i class="fas fa-phone"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="email" name="email" type="email"
                               title="Email" placeholder="Email" required>
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="password" name="password"
                               type="password" title="Password" placeholder="Пароль" required>
                        <i class="fas fa-unlock-alt"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="password_confirmation"
                               name="password_confirmation" type="password"
                               title="Password" placeholder="Подтверждение пароля" required>
                        <i class="fas fa-unlock-alt"></i>
                    </div>
                    <button type="submit">Создать аккаунт</button>
                    <div class="input-block fdr">
                        <span class="pdr-1">Есть аккаунт? </span><a href="/login"> Войти</a>
                    </div>
                </form>
            </div>

        </div>
    </main>

@endsection