@extends('layouts.dashboard')

@section('title','Главная | Дэшборд')

@section('content')

<section id="settings">
    <div class="container settings-container">
        <div class="settings-heading">
            <h1>Конфигурация системы</h1>
        </div>

        @foreach(config('variables.settings') as $key => $value)
            <div class="settings-block">
                <h3>{{ $key }}</h3>
                @foreach($value as $innerKey => $innerValue)
                    <div class="setting-row">{{ $innerKey }}: <span class="settings-value" data-toggle="modal" data-target="#settingsModal" data-category="{{ $key }}" data-title="{{ $innerKey }}"  data-value="{{ $innerValue }}">{{ $innerValue }}</span></div>
                @endforeach
            </div>
        @endforeach



    </div>

    {{--<button type="button" class="btn btn-primary" >--}}
        {{--Launch demo modal--}}
    {{--</button>--}}

                <!-- Modal -->
    <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCategory">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="settings-form">
                        <div class="form-group">
                            <label for="modalField">Email address</label>
                            <input type="text" class="form-control" id="modalField" placeholder="Введите значение" required>
                            <small id="emailHelp" class="form-text text-muted">Это поле является обязательным</small>
                        </div>
                        <button type="submit" class="btn btn-primary btn-success">Сохранить</button>
                    </form>
                </div>
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

</section>

    <script src="/js/settings.js"></script>
    <script src="/js/general.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/css/settings.css">
@endsection

