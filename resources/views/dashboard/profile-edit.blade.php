@extends('layouts.dashboard')

@section('title','Редактирование профиля | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/profile-edit.css">

    <section id="profile-edit">
        <div class="container">
            <div class="profile-edit">
                <h2>Мои данные</h2>
                <form action="/profile/edit" method="post">
                    {{ csrf_field() }}
                    <div class="form-input">
                        <label for="name">
                            <span class="required" title="Required field">
                                Имя:
                            </span>
                            </label>
                            <input type="text" name="name" id="name" required
                                   value="{{$staff->name or ''}}">
                        </div>
                        <div class="form-input">
                            <label for="surname">
                                <span class="required" title="Required field">
                                    Фамилия:
                                </span>
                            </label>
                            <input type="text" name="surname" id="surname" required
                                   value="{{$staff->surname or ''}}">
                        </div>
                        <div class="form-input">
                            <label for="address">
                                <span class="required" title="Required field">
                                    Адрес:
                                </span>
                            </label>
                            <input type="text" name="address" id="address" required
                                   value="{{$staff->address or ''}}">
                        </div>
                        <div class="form-input">
                            <label for="phone">
                                <span class="required" title="Required field">
                                    Телефон:
                                </span>
                            </label>
                            <input type="tel" name="phone" id="phone" required
                                   value="{{$staff->phone or ''}}">
                        </div>
                        <div class="form-input">
                            <label for="position">Должность: </label>
                            <p id="position">{{$staff->position or ''}}</p>
                        </div>
                        <div class="form-input">
                            <label for="salary">Оклад: </label>
                            <p id="salary">{{$staff->salary or ''}}</p>
                        </div>
                        <div class="form-input-button">
                           <button type="submit">Изменить</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
@endsection