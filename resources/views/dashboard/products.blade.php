<?php
use App\Role;
$role = new Role();
?>

@extends('layouts.dashboard')

@section('title','Продукты | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/products.css">
    <section class="products">
        <div class="container">
            <div class="products">
                <div class="table">
                    <h1>Актуальные Продукты</h1>
                    <div class="table-heading-black">
                        <div class="table-section-small">#</div>
                        <div class="table-section">Название</div>
                        <div class="table-section">Стоимость</div>
                        <div class="table-section-big">Описание</div>
                        @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                            <div class="table-section-small">
                                <i class="fas fa-edit"></i>
                            </div>
                            {{--<div class="table-section-small">--}}
                                {{--<i class="fas fa-archive"></i>--}}
                            {{--</div>--}}
                        @endif
                    </div>
                    <ul class="table-list">
                        @foreach($products as $product)
                            @if($product->actual)
                            <li class="table-list-item {{$product->id}}">
                                <div class="table-section-small">{{ $product->id or '?' }}</div>
                                <div class="table-section">{{ $product->title or '?' }}</div>
                                <div class="table-section">{{ $product->price or '?' }}</div>
                                <div class="table-section-big">{{ $product->description or '-' }}</div>

                                @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                                    <button class="table-section-small table-edit table-edit-products"
                                            id="{{ $product->id }}"
                                            onclick="togglePopupEdit();"
                                            data-token="{{ csrf_token() }}">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    {{--<button class="table-section-small table-delete table-delete-products"--}}
                                            {{--id="{{ $product->id }}"--}}
                                            {{--data-token="{{ csrf_token() }}">--}}
                                        {{--<i class="fas fa-archive"></i>--}}
                                    {{--</button>--}}
                                @endif
                            </li>
                            @endif
                        @endforeach
                    </ul>
                    {{--{!! $products->render() !!}--}}
                    @if(!$role->isStaff()) {{--RESTRICTED FOR STAFF --}}
                    <button class="table-add-block" onclick="togglePopupAdd();">
                        <i class="fas fa-plus"></i>
                        <span>Добавить</span>
                    </button>
                    @include('popup.popup-product-add')
                    @include('popup.popup-product-edit')
                    @endif
                </div>

                <div class="table products-table">

                    @foreach ($products as $product)
                        @if($product->actual)
                        <div class="accordion"><span class="title">{{ $product->title }}</span>

                            @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                            <button class="table-section-small table-edit table-edit-products"
                                    id="{{ $product->id }}"
                                    onclick="togglePopupEdit();"
                                    data-token="{{ csrf_token() }}">
                                <i class="fas fa-edit"></i>
                            </button>
                            @endif

                        </div>
                        <div class="panel">
                            <div>
                                <p>id: {{ $product->id }}</p>
                                <p>Описание: {{ $product->description }}</p>
                            </div>
                            <div>
                                <p>Ингредиенты:</p>
                                {{--FOREACH INGREDIENT IN PRODUCT--}}
                                <ul>

                                    @foreach($product_ingredient as $pi)
                                        @if($pi->id == $product->id)
                                            <li>{{ $pi->title }}, <span class="ingredient-portion">{{ $pi->description }}</span></li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                            <div>
                                <p>Стоимость: <span id="product-price">{{ $product->price }}</span></p>
                                @if(!$role->isStaff())
                                    <p>Себестоимость: <b id="product-selfprice">{{ $product->selfprice }}</b></p>
                                    <p>Доход: <span id="product-diff">{{ $product->pricedifference }}</span></p>
                                @endif
                            </div>
                            @if(!$role->isStaff())
                                <div>
                                    <p>Заказов: <b>{{ $product->alltimeorders }}</b></p>
                                    <p>Заказов за неделю: <b>{{ $product->lastweekorders }}</b></p>
                                    <p>Заказов за месяц: <b>{{ $product->lastmonthorders }}</b></p>
                                </div>
                            @endif
                        </div>
                    @endif
                    @endforeach

                </div>

                <div class="table">
                    <h1>Архивированные Продукты</h1>
                    <div class="table-heading-black">
                        <div class="table-section-small">#</div>
                        <div class="table-section">Название</div>
                        <div class="table-section">Стоимость</div>
                        <div class="table-section-big">Описание</div>
                        @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                        <div class="table-section-small">
                            <i class="fas fa-edit"></i>
                        </div>
                        <div class="table-section-small">
                            <i class="fas fa-times"></i>
                        </div>
                        @endif
                    </div>
                    <ul class="table-list">
                        @foreach($products as $product)
                            @if(!$product->actual)
                                <li class="table-list-item {{$product->id}}">
                                    <div class="table-section-small">{{ $product->id or '?' }}</div>
                                    <div class="table-section">{{ $product->title or '?' }}</div>
                                    <div class="table-section">{{ $product->price or '?' }}</div>
                                    <div class="table-section-big">{{ $product->description or '-' }}</div>

                                    @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                                    <button class="table-section-small table-edit table-edit-products"
                                            id="{{ $product->id }}"
                                            onclick="togglePopupEdit();"
                                            data-token="{{ csrf_token() }}">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    <button class="table-section-small table-delete table-delete-products"
                                            id="{{ $product->id }}"
                                            data-token="{{ csrf_token() }}">
                                        <i class="fas fa-times"></i>
                                    </button>
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    {{--{!! $products->render() !!}--}}
                    @if(!$role->isStaff()) {{--RESTRICTED FOR STAFF --}}
                    <button class="table-add-block" onclick="togglePopupAdd();">
                        <i class="fas fa-plus"></i>
                        <span>Добавить</span>
                    </button>
                    @include('popup.popup-product-add')
                    @include('popup.popup-product-edit')
                    @endif
                </div>


            </div>
        </div>
        @if(!$role->isStaff())
            <button id="button-add-general" onclick="togglePopupAdd();">+</button>
        @endif
    </section>

    <script>

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

        // Правильный вывод граммовок ингредиентов

        var ingredients = document.getElementsByClassName('ingredient-portion');
        [].forEach.call(ingredients, function(item, i, arr) {
            var locationComma = item.innerHTML.indexOf(',');
            var locationSlash = item.innerHTML.indexOf('/');
            var str = item.innerHTML.slice(locationComma+2, locationSlash);

            item.innerHTML = str;
        });


    </script>
@endsection

