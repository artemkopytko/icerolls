<?php
use App\Role;
$role = new Role();
?>

@extends('layouts.dashboard')

@section('title','Сотрудники | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/staff.css">
    <section id="staff">
        <div class="container">
            <div class="dashboard-content">
                <div class="staff table">
                    <h1>Активные сотрудники</h1>

                    <div class="table-heading-black">
                        <div class="table-section-small">#</div>
                        <div class="table-section-big">Полное Имя</div>
                        <div class="table-section">Телефон</div>
                        @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                            <div class="table-section">Оклад</div>
                        @endif
                        <div class="table-section">Адрес</div>
                        <div class="table-section">Должность</div>
                        <div class="table-section-small">
                            <i class="fas fa-check"></i>
                        </div>
                        @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                            <div class="table-section-small">
                                <i class="fas fa-edit"></i>
                            </div>
                        @endif
                    </div>
                    <ul class="table-list">
                        @foreach($staff as $s)
                            @if($s->user->activated)
                            <li class="table-list-item">
                                <div class="table-section-small">{{ $s->id or '?' }}</div>
                                <div class="table-section-big">{{ $s->name or '?' }} {{ $s->surname or '?' }}</div>
                                <div class="table-section">{{ $s->phone or '?' }}</div>
                                @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                                    <div class="table-section">{{ $s->salary or '?' }}</div>
                                @endif
                                <div class="table-section">{{ $s->address or '?' }}</div>
                                <div class="table-section">{{ $s->position or '?' }}</div>
                                <div class="table-section-small">
                                    <input type="checkbox" name="activated" value="true"
                                    @if($s->user->activated) checked @endif disabled>
                                </div>
                                @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                                    <button id="{{$s->id}}"
                                            class="table-section-small table-edit table-edit-staff"
                                            onclick="togglePopupEdit();">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                @endif
                            </li>
                            @endif
                        @endforeach
                    </ul>
                    {!! $staff->render() !!}
                    @if($role->isAdmin())
                        @include('popup.popup-staff-edit')
                    @endif
                </div>

                {{--<div class="flex-row space-between">--}}
                @if(!$role->isStaff())
                    <div class="flex-item-half">
                        <p class="flex-item-half-heading">Неактивные сотрудники</p>
                        <div class="table-light">
                            <div class="table-light-heading">
                                <div class="table-section-small">#</div>
                                <div class="table-section-big">Сотрудник</div>
                                <div class="table-section">Телефон</div>
                                <div class="table-section-small">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="table-section-small">
                                    <i class="fas fa-times"></i>
                                </div>
                            </div>
                            <ul class="table-light-content">
                                @foreach($staff as $s)
                                    @if(!$s->user->activated)
                                        <li>
                                            <div class="table-section-small">{{$s->id}}</div>
                                            <div class="table-section-big">{{$s->surname.' '.$s->name}}</div>
                                            <div class="table-section">{{$s->phone}}</div>
                                            <div id="{{$s->id}}" class="table-section-small profile-activator
                                             color-green cursor-pointer">
                                                <i class="fas fa-check"></i>
                                            </div>
                                            <div class="table-section-small table-delete table-delete-staff"
                                                 id="{{ $s->id }}"
                                                 data-token="{{ csrf_token() }}">
                                                <i class="fas fa-times"></i>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                    <div class="flex-item-half">
                        <p class="flex-item-half-heading">Рейтинг сторудников</p>
                        <div class="table-light">
                            <div class="table-light-heading">
                                <div class="table-section-small">№</div>
                                <div class="table-section-big">Сотрудник</div>
                                <div class="table-section">Заказы</div>
                                <div class="table-section">Доход</div>
                            </div>
                            <ul class="table-light-content">
                                @foreach($staffRating as $sr)
                                    <li>
                                        <div class="table-section-small">{{$sr->rank}}</div>
                                        <div class="table-section-big">{{$sr->fullname}}</div>
                                        <div class="table-section"><b>{{$sr->orders}}</b></div>
                                        <div class="table-section"><b>{{$sr->income}}</b></div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                {{--</div>--}}
                {{--<div class="flex-row space-between">--}}

                    <div class="flex-item-half">
                        <p class="flex-item-half-heading">Рабочие часы сотрудников</p>
                        <div class="table-light">
                            <div class="table-light-heading">
                                <div class="table-section-big">Сотрудник</div>
                                <div class="table-section">Часы</div>
                                <div class="table-section">Смены</div>
                            </div>
                            <ul class="table-light-content">
                                @foreach($staffUptime as $sut)
                                    <li>
                                        <div class="table-section-big">{{$sut->fullname}}</div>
                                        <div class="table-section"><b>{{$sut->hours}}</b></div>
                                        <div class="table-section"><b>{{$sut->days}}</b></div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                {{--</div>--}}
            </div>
        </div>
    </section>

    <script>
        var x = $('div #2 .profile-activator');
        console.log(x);
    </script>
@endsection

