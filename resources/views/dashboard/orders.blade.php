<?php
use App\Role;
$role = new Role();
use Carbon\Carbon;
?>

@extends('layouts.dashboard')

@section('title','Заказы | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/orders.css">

    @if(!$role->isStaff())


    <div class="container fdc">
        <div class="charts">
            <div class="accordion">
                <span class="title">Общие графики</span>
            </div>
            <div class="panel panel-orders">
                <div class="chart">
                    <div class="chart-content">
                        <h2>Заказы за последний месяц</h2>
                        <canvas id="canvas-generalMonthChart"></canvas>
                    </div>
                </div>
                <div class="chart">
                    <div class="chart-content">
                        <h2>Заказы за последнюю неделю</h2>
                        <canvas id="canvas-generalWeekChart"></canvas>
                    </div>
                </div>
                <div class="chart">
                    <div class="chart-content">
                        <h2>Доходы за последний месяц</h2>
                        <canvas id="canvas-lastMonthIncomes"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="charts mt15">
            <div class="accordion">
                <span class="title">Детализированные графики</span>
            </div>
            <div class="panel panel-orders">
                <div class="chart-big">
                    <div class="chart-content">
                        <h2>Детальные заказы за неделю</h2>
                        <canvas id="canvas-detailedWeekChart"></canvas>
                    </div>
                </div>
                <div class="chart-big">
                    <div class="chart-content">
                        <h2>Детальные заказы за месяц</h2>
                        <canvas id="canvas-detailedMonthChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="charts mt15">
            <div class="accordion">
                <span class="title">Статистика по трендам</span>
            </div>
            <div class="panel panel-orders">
                <div class="chart-big">
                    <div class="chart-content">
                        <h2>Тренды за <?php echo Carbon::now()->year;?> год</h2>
                        <canvas id="canvas-thisYearMonthSales"></canvas>
                    </div>
                </div>
                <div class="chart">
                    <div class="chart-content">
                        <h2>Заказы за <?php echo Carbon::now()->year;?> год</h2>
                        <canvas id="canvas-thisYearMonthSalesDoughnut"></canvas>
                    </div>
                </div>
            </div>

<div>Постороить график</div>
            <form action="">
                <select name="" id="">
                    <option value=""></option>
                    <option value=""></option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
            </form>

        </div>
    </div>

    <section id="stats">
        <div class="container">
            <div class="stats">
                <h1>Подсчеты по продажам</h1>
                <div class="total-stats">
                    <hr>
                    <div class="container fdc">
                        <h2 class="stats-title">Все время</h2>
                        <p>Заказов за все время: {{ $allTimeStats[0]->orders }}</p>
                        <p>Доход за все время: {{ $allTimeStats[0]->income }}</p>
                        <p>Затрачено на производство: {{ $allTimeStats[0]->selfprice }}</p>
                        <p>Прибыль за все время: {{ $allTimeStats[0]->pureincome }}</p>
                    </div>
                    <hr>

                    {{--1) TOTAL INCOME --}}
                    {{--2) TOLOTAL SELFPRICE --}}
                    {{--3) TOTOAL ORDERS --}}
                    {{--4) TOTAL ORDER-PRODUCT-RANK --}}
                </div>
                <div class="month-stats">
                    <div class="container fdc">
                        <h2 class="stats-title">Прошедший месяц</h2>
                        <p>Заказов за все время: {{ $monthStats[0]->orders }}</p>
                        <p>Доход за все время: {{ $monthStats[0]->income }}</p>
                        <p>Затрачено на производство: {{ $monthStats[0]->selfprice }}</p>
                        <p>Прибыль за все время: {{ $monthStats[0]->pureincome }}</p>
                    </div>
                    <hr>
                </div>
                <div class="week-stats">
                    <div class="container fdc">
                        <h2 class="stats-title">Прошедшая неделя</h2>
                        <p>Заказов за все время: {{ $weekStats[0]->orders }}</p>
                        <p>Доход за все время: {{ $weekStats[0]->income }}</p>
                        <p>Затрачено на производство: {{ $weekStats[0]->selfprice }}</p>
                        <p>Прибыль за все время: {{ $weekStats[0]->pureincome }}</p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <script>

        //        AJAX FOR CHARTS
        //        1 CHART MONTH | ORDERS DONE
        //        2 CHART LAST 7 DAYS ORDERS
        //        3 CHART LAST 7 DAYS BY PRODUCT
        //        TOTAL INCOME
        //        TOTAL ORDERS
        //        LAST MONTH ORDERS
        //        LAST WEEK ORDERS
        //        TODAY ORDERS

    </script>
    @endif
    <section id="orders">
        <div class="container">
            <div class="orders table">
                <h1>Заказы</h1>
                <div class="table-heading-black">
                    <div class="table-section-small">#</div>
                    {{--<div class="table-section">Сотрудник</div>--}}
                    <div class="table-section">Продукт</div>
                    <div class="table-section-small">Кол-во</div>
                     <div class="table-section-small">%</div>
                    {{--<div class="table-section">Дата</div>--}}
                    <div class="table-section-small">Сумма</div>
                    <div class="table-section-small">Итого</div>
                    <div class="table-section-small">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <ul class="table-list">
                    @foreach($orders as $order)
                        <div class="accordion position" data-id="{{$order->id}}">
                            <span class="title">#{{$order->id or '?'}}</span>
                        </div>
                        <div class="panel panel-position-inner">
                            {{--<div class="table-list-item">--}}
                                {{--<div class="table-section-small">#</div>--}}
                                {{--<div class="table-section">Ice Cream Ice Cream</div>--}}
                                {{--<div class="table-section-small">1</div>--}}
                                {{--<div class="table-section-small">10%</div>--}}
                                {{--<div class="table-section-small">55</div>--}}
                                {{--<div class="table-section-small">44</div>--}}
                                {{--<div class="table-section-small">--}}
                                    {{--<i class="fas fa-times"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="table-list-item">--}}
                                {{--<div class="table-section-small">#</div>--}}
                                {{--<div class="table-section">Ice Cream Ice Cream</div>--}}
                                {{--<div class="table-section-small">1</div>--}}
                                {{--<div class="table-section-small">10%</div>--}}
                                {{--<div class="table-section-small">55</div>--}}
                                {{--<div class="table-section-small">44</div>--}}
                                {{--<div class="table-section-small">--}}
                                    {{--<i class="fas fa-times"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="table-list-item">--}}
                                {{--<div class="table-section-small">#</div>--}}
                                {{--<div class="table-section">Ice Cream Ice Cream</div>--}}
                                {{--<div class="table-section-small">1</div>--}}
                                {{--<div class="table-section-small">10%</div>--}}
                                {{--<div class="table-section-small">55</div>--}}
                                {{--<div class="table-section-small">44</div>--}}
                                {{--<div class="table-section-small">--}}
                                    {{--<i class="fas fa-times"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="table-list-item">--}}
                                {{--<div class="table-section-small">#</div>--}}
                                {{--<div class="table-section">Ice Cream Ice Cream</div>--}}
                                {{--<div class="table-section-small">1</div>--}}
                                {{--<div class="table-section-small">10%</div>--}}
                                {{--<div class="table-section-small">55</div>--}}
                                {{--<div class="table-section-small">44</div>--}}
                                {{--<div class="table-section-small">--}}
                                    {{--<i class="fas fa-times"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="table-list-item">--}}
                                {{--<div class="table-section-small">#</div>--}}
                                {{--<div class="table-section">Ice Cream Ice Cream</div>--}}
                                {{--<div class="table-section-small">1</div>--}}
                                {{--<div class="table-section-small">10%</div>--}}
                                {{--<div class="table-section-small">55</div>--}}
                                {{--<div class="table-section-small">44</div>--}}
                                {{--<div class="table-section-small">--}}
                                    {{--<i class="fas fa-times"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>
                    @endforeach

                    {{--
                    @foreach($orders as $order)
                        <li class="table-list-item {{$order->id}}">
                            <div class="table-section-small">{{ $order->id or '?' }}</div>
                            <div class="table-section">{{ $order->staff->name.' '.$order->staff->surname  }}</div>
                            <div class="table-section">{{ $order->product->title }}</div>
                            <div class="table-section-small">{{ $order->quantity or '?' }} </div>
                             <div class="table-section-small">
                                 @php
                                 if($order->discount)
                                    {echo '-'.$order->discount.'%';}
                                 else
                                 	{echo '-';}
                                 @endphp
                             </div>
                            <div class="table-section">
                                @if($order->date)
									@php $dt = $order->date; @endphp
                                    {{  $dt->toFormattedDateString().' '.$dt->toTimeString()}}
                                @else
                                    -
                                @endif
                            </div>
                            <div class="table-section">{{ ($order->product->price * $order->quantity) - ($order->product->price * $order->quantity * $order->discount / 100)}}</div>
                            <button class="table-section-small table-delete table-delete-orders"
                                    id="{{ $order->id }}"
                                    data-token="{{ csrf_token() }}">
                                <i class="fas fa-times"></i>
                            </button>
                        </li>
                    @endforeach
                    --}}
                </ul>

                <button class="table-add-block" onclick="togglePopupAdd();">
                    <i class="fas fa-plus"></i>
                    <span>Добавить</span>
                </button>

                @include('popup.popup-order-edit')
                @include('popup.popup-order-add')

                {!! $orders->render() !!}
            </div>
        </div>
        <button id="button-add-general" onclick="togglePopupAdd();">+</button>
    </section>
    <script src="/js/general.js"></script>
    <script src="/js/Chart.bundle.min.js"></script>
    <script src="/js/orderCharts.js"></script>
    <script>
        $(document).ready(function(){

            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }

            @if(!$role->isStaff())
                generalMonthChart();
                generalWeekChart();
                lastMonthIncomes();
                detailedWeekChart();
                detailedMonthChart();
                thisYearMonthSales();
            @endif

            $('.position').on('click', function () {
                event.preventDefault();
                var id = $(this).data('id');
                var row = $(this);
                if(row.hasClass('active')) {
                    console.log('Searching ID: ', id);
                    $.ajaxSetup({
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        dataType: 'json',
                        type: "post",
                        url: '/orders/getOrderPositions/' + id,
                        data: {id: id},
                        success: function (response) {
                            row.next().empty();
                            console.log(response);
                            showSuccessMessage('Чек #'+id);

                            var sum = 0;
                            var selfprice = 0;
                            var staff = [];

                            for (var i = 0; i < response.positions.length; i += 1) {
                                var title = response.positions[i].title;
                                var quantity = response.positions[i].quantity;
                                var discount = response.positions[i].discount;
                                var price = response.positions[i].price;
                                var positionPrice = (price * quantity) - (price * quantity * discount / 100);
                                selfprice += parseFloat(response.positions[i].selfprice) * quantity;
                                console.log(selfprice);
                                sum += positionPrice;
                                staff.push(response.positions[i].staffname);
                                console.log(response.positions[i]);
                                var position = '<div class="table-list-item">\n' +
                                    '                                <div class="table-section-small">#' + i + '</div>\n' +
                                    '                                <div class="table-section">' + title + '</div>\n' +
                                    '                                <div class="table-section-small">' + quantity + '</div>\n' +
                                    '                                <div class="table-section-small">' + discount + '%</div>\n' +
                                    '                                <div class="table-section-small">' + positionPrice + '</div>\n' +
                                    '                                <div class="table-section-small">' + sum + '</div>\n' +
                                    '                                <div class="table-section-small">\n' +
                                    '                                    <i class="fas fa-times"></i>\n' +
                                    '                                </div>\n' +
                                    '                            </div>';
                                row.next().append(position);
                            }

                            row.next().append('<hr>');

                            selfprice = selfprice.toFixed(2);

                            var income = (sum - selfprice).toFixed(2);

                            var checkTotals = '<div class="order-position-total"><span>Итого: </span><span><b>' + sum + '₴</b></span></div>' +
                                '<div class="order-position-total"><span>Расход: </span><span><b>' + selfprice + '₴</b></span></div>' +
                                '<div class="order-position-total"><span>Прибыль: </span><span><b>' + income + '₴</b></span></div>';
                            row.next().append(checkTotals);


                            function onlyUnique(value, index, self) {
                                return self.indexOf(value) === index;
                            }

                            var uniqueStaff = staff.filter(onlyUnique);


                            row.next().append('<div>' +
                                '<p>Сотрудник: ' + uniqueStaff + '</p><p>Время: ' + response.positions[response.positions.length - 1].date + '</p></div>')

                            if (row.hasClass('active')) {
                                row.next().css('maxHeight', row.next().prop('scrollHeight') + "px");
                            }
//                        var panel = acc.nextElementSibling;
//                        if (row.next().style.maxHeight){
//                            row.next().css('maxHeight', row.next().scrollHeight + "px");
//                        }
//                        response.positions.each(function(index) {
//                            var position = '<div>{index}</div>';
//                            console.log($(this));
//                        });
                        }

                    });
                }
            })
        });
    </script>
@endsection

