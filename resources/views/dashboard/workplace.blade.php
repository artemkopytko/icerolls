<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/workplace.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <meta name="csrf-token" content="<?=csrf_token()?>">
    <script src="/js/jQuery.js"></script>
</head>
<body>
{{--TODO: MESSAGES DISAPEARING POPUP--}}
<div id="messages" class="messages">
    {{--<div class="success">POPUP success</div>--}}
    {{--<div class="fail">Popup fail</div>--}}
</div>
<nav class="nav">
    <div class="nav-name">
        <span id="current-user" data-user-id="{{auth()->user()->staff->id}}">{{auth()->user()->staff->name}}</span>
    </div>
    <div class="nav-time">
        <span id="date"></span>
        <span id="time"></span>
    </div>
    <div class="nav-orders">
        {{--TODO: Создать ХП для вывода заказов за сегодня--}}
        <span id="today-orders">Заказов: 23</span>
    </div>
    <div class="nav-status">
        <span id="status-indicator" class="status-indicator-ok"></span>
    </div>
</nav>
<main id="workplace">
    <section id="check-preview" class="check-preview">
        <div id="check-content" class="check-content">
            {{--
            THE ORDER IS PRINTED HERE BY workplace.js
            <div class="row" data-order-position-id="i">
                <div class="cell-small">#</div>
                <div class="cell-big">Название</div>
                <div class="cell-small">Кол-во</div>
                <div class="cell">Сумма</div>
            </div>
            --}}
        </div>
        <div class="order-total-row">
            <div class="cell-small">Итого</div>
            <div class="cell-big"></div>
            <div class="cell-small"></div>
            <div class="cell" id="order-total"></div>
        </div>
        <div class="check-etc">
            <button title="Выход" class="exit-button control-operation-button" onclick="exit(event);" id="exit-button-link"><i class="fas fa-sign-out-alt"></i></button>
            <button title="Обновить" class="update-button control-operation-button" onclick="resetView();" id="update-button-link"><i class="fas fa-sync-alt"></i></button>
            {{--<button title="Смена пользователя" class="change-user-button control-operation-button" onclick="javascript(0);" id="change-user-button"><i class="fas fa-users"></i></button>--}}
            {{--<button title="Заблокировать" class="lock-button control-operation-button" onclick="javascript(0);" id="lock-button"><i class="fas fa-lock"></i></button>--}}
        </div>
    </section>
    <section class="menu">
        <div class="menu-upper">
            <div id="categories-content" class="categories-content">
                {{--
                CATEGORIES ARE PRINTED HERE BY workplace.js
                <div class="category">
                    <button
                    data-category-name="Category Name"
                     class="category-button">Category Name</div>
                </div>

                --}}
            </div>
        </div>
        <div class="menu-lower">
            <div id="products-content" class="products-content">
                {{--
                                PRODUCTS ARE PRINTED HERE BY workplace.js
                                <div class="product">
                                    <button
                                    data-product-id="1"
                                     class="product-button">Product Title</div>
                                </div>

                                --}}
            </div>
        </div>
    </section>
    <section class="order-control">
        <div class="order-upper">
            <div id="order-content" class="order-content">

            </div>
        </div>
        <div class="order-lower">
            <div id="product-portions" class="product-portions">
                <button class="portion-number-button">1</button>
                <button class="portion-number-button">2</button>
                <button class="portion-number-button">3</button>
                <button class="portion-number-button">4</button>
                <button class="portion-number-button">5</button>
                <button class="portion-number-button">6</button>
            </div>
            <div class="order-controls">
                <button id="order-add" onclick="orderAdd();">✓</button>
                <button id="order-cancel-product" onclick="orderCancel();">←</button>
            </div>
            <button id="order-submit" class="order-submit-button"><i class="fas fa-plus-circle"></i></button>
        </div>
    </section>

    <div class="popup-wrapper">
        <div class="workplace-popup-form">
            <button id="popup-close" onclick="togglePopup()">
                <i class="fas fa-times"></i>
                {{--<img src="/images/workplace/cross.svg" alt="">--}}
            </button>
            <div class="popup-form-info">
                <form action="">
                    <div class="workplace-popup-form-item">
                        <span for="popup-id">Позиция в чеке:  №</span>
                        <p id="popup-id" type="text" disabled></p>
                    </div>
                    <div class="workplace-popup-form-item">
                        <span for="popup-name">Название: </span>
                        <p id="popup-name" type="text" disabled></p>
                    </div>
                    <div class="workplace-popup-form-item"  data-action="стоимости">
                        <span for="popup-price">Стоимость: </span>
                        <p id="popup-price" class="popup-field-controlled"></p>
                        <div class="locked-icon"><i class="fas fa-lock"></i></div>
                    </div>
                    <div class="workplace-popup-form-item editable locked" data-action="количества порций">
                        <span for="popup-quantity">Единиц в заказе:</span>
                        <p id="popup-quantity" class="popup-field-controlled" onchange="quantityEdited();" data-price="0"></p>
                        <div class="locked-icon"><i class="fas fa-lock"></i></div>
                    </div>
                    <div class="workplace-popup-form-item editable locked" data-action="суммы">
                        <span for="popup-sum">Сумма:</span>
                        <p id="popup-sum" class="popup-field-controlled"></p>
                        <div class="locked-icon"><i class="fas fa-lock"></i></div>
                    </div>
                </form>
                <button id="allowEditing" data-allowed="false" class="locked">Разрешить редактирование
                    <div class="locked-icon"><i class="fas fa-lock"></i></div>
                    <div class="unlocked-icon"><i class="fas fa-unlock"></i></div>
                </button>

            </div>
            <div class="popup-form-controls">
                <div class="popup-form-controls-nums">
                    <h2 id="popup-form-controls-action" class="popup-form-controls-action"></h2>
                    <p id="popup-form-controls-value"></p>
                    <div class="popup-form-action-value-controls" id="popup-form-quantity-controls">
                        <button id="popup-form-action-quantity-plus"><i class="fas fa-plus"></i></button>
                        <button id="popup-form-action-quantity-minus"><i class="fas fa-minus"></i></button>
                    </div>
                    <div class="popup-form-action-value-controls" id="popup-form-discount-controls">
                        <?
                        $discounts = config('variables.discounts');

                        foreach ($discounts as $discount) {
                        	echo '<button class="popup-form-discount-button" data-discount="'.$discount.'">-'.$discount.'%</button>';
                        }
                        ?>
                    </div>
                </div>
                <div class="popup-form-controls-actions">
                    {{--<button id="popup-edit" onclick="allowQuantityEdit();">--}}
                        {{--<img src="" alt="">Редактировать</button>--}}
                    <button id="popup-delete" onclick="deleteOrderPosition();">
                        <img src="" alt="">Удалить</button>
                </div>
            </div>
            {{--TODO: DISCOUNT--}}
            {{--<button>Скидка</button>--}}
          </div>
    </div>

    <div class="popup-order-wrapper">
        <div class="workplace-order-popup-form">

            <h2>Управление заказом</h2>
            <button id="popup-order-close">
                <i class="fas fa-times"></i>
                {{--<img src="/images/workplace/cross.svg" alt="">--}}
            </button>
            <div id="order-popup-positions"></div>
            <p id="order-popup-total"></p>
            <hr>
            <div class="order-popup-controls">
                <button id="order-popup-discount">Добавить скидку к заказу</button>
                <button id="order-popup-clear">Очистить заказ</button>
            </div>
            <div class="order-popup-discount-buttons">
	            <?
	            $discounts = config('variables.discounts');

	            foreach ($discounts as $discount) {
		            echo '<button class="order-popup-form-discount-button" data-discount="'.$discount.'">-'.$discount.'%</button>';
	            }
	            ?>
            </div>
            {{--<div class="popup-form-info">--}}
                {{--<form action="">--}}

                {{--</form>--}}
            {{--</div>--}}
            {{--TODO: DISCOUNT--}}
            {{--<button>Скидка</button>--}}
        </div>
    </div>


</main>
<script src="/js/general.js"></script>
<script src="/js/workplaceCombined.js"></script>
{{--<script src="/js/workplace_queries.js"></script>--}}
{{--<script src="/js/workplace.js"></script>--}}
{{--<script src="/js/workplace_general.js"></script>--}}
</body>
</html>


