<?php
use App\Role;
$role = new Role();

?>



@extends('layouts.dashboard')

@section('title','Ингредиенты | Дэшборд')
@section('content')
    <section id="ingredients">
        <div class="container">
            <div class="ingredients table">
                <h1>Ингредиенты</h1>
                <div class="table-heading-black">
                    <div class="table-section-small">#</div>
                    <div class="table-section">Название</div>
                    <div class="table-section">Стоимость</div>
                    <div class="table-section">Стоимость/ед</div>
                    <div class="table-section">Ед. в упаковке</div>
                    <div class="table-section-big">Описание</div>
                    @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                        <div class="table-section-small">
                            <i class="fas fa-edit"></i>
                        </div>
                        <div class="table-section-small">
                            <i class="fas fa-times"></i>
                        </div>
                    @endif
                </div>
                <ul class="table-list">
                    @foreach($ingredients as $ingredient)

                        <li class="table-list-item {{$ingredient->id}}">

                            <div class="table-section-small">{{ $ingredient->id or '?' }}</div>
                            <div class="table-section">{{ $ingredient->title or '?' }}</div>
                            <div class="table-section">{{ $ingredient->price or '?' }}</div>
                            <div class="table-section">
                                {{--*/ $ingredient->price /*--}}
                                <?php echo round( $ingredient->price / $ingredient->amountperpack , 2);?>

                            </div>
                            <div class="table-section">{{ $ingredient->amountperpack or '?' }}</div>
                            <div class="table-section-big">{{ $ingredient->description or '-' }}</div>

                            @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                                <button class="table-section-small table-edit table-edit-ingredients"
                                        id="{{$ingredient->id}}"
                                        onclick="togglePopupEdit();">
                                    <i class="fas fa-edit"></i>
                                </button>

                            <button class="table-section-small table-delete table-delete-ingredients"
                                    id="{{ $ingredient->id }}"
                                    data-token="{{ csrf_token() }}">
                                <i class="fas fa-times"></i>
                            </button>
                            @endif
                        </li>

                    @endforeach
                </ul>
                {!! $ingredients->render() !!}
                @if($role->isAdmin())
                    <button class="table-add-block" onclick="togglePopupAdd();">
                        <i class="fas fa-plus"></i>
                        <span>Добавить</span>
                    </button>
                    @include('popup.popup-ingredient-edit')
                    @include('popup.popup-ingredient-add')
                @endif

            </div>
        </div>
        @if(!$role->isStaff())
            <button id="button-add-general" onclick="togglePopupAdd();">+</button>
        @endif
    </section>

@endsection

