@extends('layouts.dashboard')

@section('title','Профиль | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/profile.css">

    <section id="profile">
        <div class="container">
            <div class="profile">
                <h2>Мои данные</h2>
                <div class="profile-info">
                    <p>Имя: {{$staff->name or 'Неизвестно'}}</p>
                </div>
                <div class="profile-info">
                    <p>Фамилия: {{$staff->surname or 'Неизвестно'}}</p>
                </div>
                <div class="profile-info">
                    <p>Адрес: {{$staff->address or 'Неизвестно'}}</p>
                </div>
                <div class="profile-info">
                    <p>Телефон: {{$staff->phone or 'Неизвестно'}}</p>
                </div>
                <div class="profile-info">
                    <p>Должность: {{$staff->position or 'Неизвестно'}}</p>
                </div>
                <div class="profile-info">
                    <p>Оклад:
                        @if($staff->salary)
                            {{ $staff->salary }}
                        @else
                            <span class="tooltip">{{$staff->salary or 'Неизвестно'}}
                                <span class="tooltiptext">Вскоре будет установлен</span>
                            </span>
                        @endif
                    </p>
                </div>
                <div class="profile-info-edit">
                    <a href="/profile/edit">Редактировать</a>
                </div>
            </div>
        </div>
    </section>

@endsection