<?php
use App\Role;
use App\Income;
$role = new Role();

$income = new Income;

?>



@extends('layouts.dashboard')

@section('title','Доходы | Дэшборд')

@section('content')
    {{--<script src="/js/payments.js"></script>--}}
    <link rel="stylesheet" href="/css/payments.css">

    <section id="stats">
        <div class="container">
            <div class="stats">
                <h1>Подсчеты по продажам</h1>
                <div class="total-stats">
                    <hr>
                    <div class="container fdc">
                        <h2 class="stats-title">Доходы</h2>
                        <p>Доход за все время: @foreach($income->totalIncome() as $item)
                            {{ $item->sum }}
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section id="payments">
        <div class="container fdc">
            <div class="payments table">
                <h1>Доходы</h1>
                <div class="table-heading-black">
                    <div class="table-section-small">#</div>
                    <div class="table-section">Сума</div>
                    <div class="table-section-big">Комментарий</div>
                    <div class="table-section">Дата</div>
                    @if(!$role->isStaff()) {{-- RESTICTED FOR STAFF --}}
                    <div class="table-section-small">
                        <i class="fas fa-edit"></i>
                    </div>
                    <div class="table-section-small">
                        <i class="fas fa-times"></i>
                    </div>
                    @endif
                </div>
                <ul class="table-list">

                    @foreach($incomes as $income)
                        <li class="table-list-item {{$income->id}}">

                            <div class="table-section-small">{{ $income->id or '?' }}</div>
                            <div class="table-section">{{ $income->amount or '?' }}</div>
                            <div class="table-section-big">{{ $income->note }}</div>
                            <div class="table-section">{{ $income->date->toFormattedDateString() }}</div>
                            @if(!$role->isStaff()) {{-- RESTICTED FOR STAFF --}}
                            <button class="table-section-small table-edit table-edit-incomes"
                                    id="{{$income->id}}"
                                    onclick="togglePopupEdit();">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button class="table-section-small table-delete table-delete-incomes"
                                    id="{{ $income->id }}"
                                    data-token="{{ csrf_token() }}">
                                <i class="fas fa-times"></i>
                            </button>
                            @endif
                        </li>

                    @endforeach

                </ul>
                {!! $incomes->render() !!}
                @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                <button class="table-add-block" onclick="togglePopupAdd();">
                    <i class="fas fa-plus"></i>
                    <span>Добавить</span>
                </button>
                @include('popup.popup-income-edit')
                @include('popup.popup-income-add')
                @endif
            </div>
        </div>
        @if(!$role->isStaff())
            <button id="button-add-general" onclick="togglePopupAdd();">+</button>
        @endif
    </section>

@endsection

