<?php
use App\Role;
$role = new Role();
$min_salary = config('variables.settings.Платежная информация.Минимальная зарплата');
$taxPercent = '0.'.config('variables.settings.Платежная информация.Ставка ЕСВ');
$min_tax = $min_salary * $taxPercent;
?>

@extends('layouts.dashboard')

@section('title','Платежи | Дэшборд')

@section('content')
    <script src="/js/general.js"></script>
    <script src="/js/payments.js"></script>
    <link rel="stylesheet" href="/css/payments.css">
    <section id="payments">
        <div class="container fdc">
            <div class="taxes">
                    <h1>Отчетность по платежам</h1>
                <div class="tax-block">
                    <h3>Налоговая отчетность</h3>
                    <br>
                    <div class="accordion taxBookLastMonthWrapper" data-token="{{csrf_token()}}"><span>Доходы за последние 30 дней</span></div>
                    <div class="panel">
                        <div class="small-table">
                            <div class="small-table-row">
                                <div class="small-table-cell">Дата</div>
                                <div class="small-table-cell">Доход</div>
                            </div>
                            <div class="small-table-content taxBookLastMonth">
                            </div>
                        </div>
                    </div>

                    <div class="accordion taxBookCustomPeriodSumWrapper" data-token="{{csrf_token()}}"><span>Рассчет дохода за промежуток времени</span></div>
                    <div class="panel">
                        <form class="taxBookCustomPeriodSumForm" action="">
                            <div class="form-row">
                                <label for="customPeriodSumStart">Дата начала <small>(включительно)</small>:</label>
                                <input type="date" required id="customPeriodSumStart" value="<?echo Date('Y-m-d', strtotime("-7 days"))?>">
                            </div>
                            <div class="form-row">
                                <label for="customPeriodSumFinish">Дата конца <small>(включительно)</small>:</label>
                                <input type="date" required id="customPeriodSumFinish" value="<?echo date('Y-m-d');?>" max="<?echo date('Y-m-d');?>">
                            </div>
                            <div class="form-row">
                                <button>Рассчитать</button>
                            </div>
                        </form>


                            <p class="taxBookCustomPeriodSum"></p>
                    </div>

                    <div class="accordion taxBookGetMonthlyIncomesWrapper" data-token="{{csrf_token()}}"><span>Доходы по месяцам</span></div>
                    <div class="panel">
                        <div class="small-table">
                            <div class="small-table-row">
                                <div class="small-table-cell">Дата</div>
                                <div class="small-table-cell">Доход</div>
                            </div>
                            <div class="small-table-content taxBookGetMonthlyIncomes">
                            </div>
                        </div>
                    </div>

                    <div class="accordion latestTaxes"><span>Последние уплаты налогов</span></div>
                    <div class="panel">
                        <div class="small-table">
                            <div class="small-table-row">
                                <div class="small-table-cell">Тип</div>
                                <div class="small-table-cell">Дата Оплаты</div>
                                <div class="small-table-small-cell">Сумма</div>
                                <div class="small-table-cell">Дата следующей оплаты:</div>
                            </div>
                            <div class="small-table-content">
                                <div class="small-table-row">
                                    @if(count($latestEN)>0)
                                        <div class="small-table-cell">{{ $latestEN[0]->type }}</div>
                                        <div class="small-table-cell">{{ $latestEN[0]->date }}</div>
                                        <div class="small-table-small-cell">{{ $latestEN[0]->amount }}</div>
                                        <div class="small-table-cell danger-color">{{ $latestEN[0]->valid_till }}</div>
                                    @else
                                    @endif
                                </div>
                                <div class="small-table-row">
                                    @if(count($latestESV)>0)
                                        <div class="small-table-cell">{{ $latestESV[0]->type }}</div>
                                        <div class="small-table-cell">{{ $latestESV[0]->date }}</div>
                                        <div class="small-table-small-cell">{{ $latestESV[0]->amount }}</div>
                                        <div class="small-table-cell danger-color">{{ $latestESV[0]->valid_till }}</div>
                                    @else
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accordion latestTaxes"><span>Отчет по ЕСВ за год</span></div>
                    <div class="panel">
                        <div class="small-table">
                            <div class="small-table-row">
                                <div class="small-table-cell">Дата Оплаты</div>
                                <div class="small-table-cell">Мин. зарплата</div>
                                <div class="small-table-small-cell">%</div>
                                <div class="small-table-cell">Сумма</div>
                            </div>
                            <div class="small-table-content">
                                @foreach($thisYearTaxes_ESV as $ESV_tax)
                                    <div class="small-table-row">
                                        <div class="small-table-cell">{{ $ESV_tax->date }}</div>
                                        <div class="small-table-cell"><?php echo $min_salary;?></div>
                                        <div class="small-table-small-cell"><?= $taxPercent;?></div>
                                        <div class="small-table-cell"><b>{{ $ESV_tax->amount }}</b>/<?=$min_tax;?></div>
                                    </div>
                                @endforeach
                                @if(count($thisYearTaxes_ESV_total)>0)
                                    <div class="small-table-row">
                                        <div class="small-table-cell">Итого: </div>
                                        <div class="small-table-cell">{{ $thisYearTaxes_ESV_total[0]->sum }}</div>
                                        <div class="small-table-small-cell"></div>
                                        <div class="small-table-cell"></div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <br>

                    <div>
                        <p>Минимальная зарплата в <?echo date('Y')?> году - <b><?echo $min_salary?></b>грн.</p>
                        <p>Сумма единого социального взноса составляет <?echo $min_salary * $taxPercent?> грн.</p>
                    </div>

                </div>


                <div class="tax-block">
                    <h3>Данные о платежах</h3>
                    <br>
                    <div class="accordion latestTaxes"><span>Последняя уплата аренды</span></div>
                    <div class="panel">
                        <div class="small-table">
                            <div class="small-table-row">
                                <div class="small-table-cell">Тип</div>
                                <div class="small-table-cell">Дата Оплаты</div>
                                <div class="small-table-small-cell">Сумма</div>
                                <div class="small-table-cell">Дата следующей оплаты:</div>
                            </div>
                            <div class="small-table-content">
                                <div class="small-table-row">
                                    @if(count($latestRent)>0)
                                    <div class="small-table-cell">{{ $latestRent[0]->type }}</div>
                                    <div class="small-table-cell">{{ $latestRent[0]->date }}</div>
                                    <div class="small-table-small-cell">{{ $latestRent[0]->amount }}</div>
                                    <div class="small-table-cell danger-color">{{ $latestRent[0]->valid_till }}</div>
                                        @else
                                    <div></div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function(){

                    var acc = document.getElementsByClassName("accordion");
                    var i;

                    for (i = 0; i < acc.length; i++) {
                        acc[i].addEventListener("click", function() {
                            this.classList.toggle("active");
                            var panel = this.nextElementSibling;
                            if (panel.style.maxHeight){
                                panel.style.maxHeight = null;
                            } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                            }
                        });
                    }

                    var taxBookWrapperExpanded = false;
                    var taxBookLastMonthFetched = false;
                    var taxBookGetMonthlyIncomesFetched = false;

//                    $('.taxBookWrapper').on('click',function () {
//                        if(!taxBookWrapperExpanded) {
//                            taxBookWrapperExpanded = !taxBookWrapperExpanded;
//                            taxBook();
//                        }
//                    })


                    $('.taxBookLastMonthWrapper').on('click', function () {
                        if(!taxBookLastMonthFetched) {
                            taxBookLastMonth();
                            taxBookLastMonthFetched = true;
                        }
                    });

                    $('.taxBookCustomPeriodSumForm').on('submit', function () {
                        taxBookCustomPeriodSum();
                    });

                    $('.taxBookGetMonthlyIncomesWrapper').on('click', function () {
                        if(!taxBookGetMonthlyIncomesFetched) {
                            taxBookGetMonthlyIncomes();
                            taxBookGetMonthlyIncomesFetched = true;
                        }
                    });

                });
            </script>

            <div class="payments table">
                <h1>Платежи</h1>
                <div class="table-heading-black">
                    <div class="table-section-small">#</div>
                    <div class="table-section">Вид</div>
                    <div class="table-section">Сумма</div>
                    <div class="table-section-big">Комментарий</div>
                    <div class="table-section">Дата</div>
                    <div class="table-section">Действительно до</div>
                    @if(!$role->isStaff()) {{-- RESTICTED FOR STAFF --}}
                        <div class="table-section-small">
                            <i class="fas fa-edit"></i>
                        </div>
                        <div class="table-section-small">
                            <i class="fas fa-times"></i>
                        </div>
                    @endif
                </div>
                <ul class="table-list">

                    @foreach($payments as $payment)
                        <li class="table-list-item {{$payment->id}}">

                            <div class="table-section-small">{{ $payment->id or '?' }}</div>
                            <div class="table-section">{{ $payment->type or '?' }}</div>
                            <div class="table-section">{{ $payment->amount or '?' }}</div>
                            <div class="table-section-big">{{ $payment->note or '-' }}</div>
                            <div class="table-section">{{ $payment->date or '?' }} </div>
                            <div class="table-section">{{ $payment->valid_till or '-' }}</div>
                            @if(!$role->isStaff()) {{-- RESTICTED FOR STAFF --}}
                                <button class="table-section-small table-edit table-edit-payments"
                                     id="{{$payment->id}}"
                                     onclick="togglePopupEdit();">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button class="table-section-small table-delete table-delete-payments"
                                        id="{{ $payment->id }}"
                                        data-token="{{ csrf_token() }}">
                                    <i class="fas fa-times"></i>
                                </button>
                            @endif
                        </li>

                    @endforeach

                </ul>
                {!! $payments->render() !!}
                @if(!$role->isStaff()) {{-- RESTRICTED FOR STAFF --}}
                    <button class="table-add-block" onclick="togglePopupAdd();">
                        <i class="fas fa-plus"></i>
                        <span>Добавить</span>
                    </button>
                    @include('popup.popup-payment-edit')
                    @include('popup.popup-payment-add')
                @endif
            </div>
        </div>
        @if(!$role->isStaff())
            <button id="button-add-general" onclick="togglePopupAdd();">+</button>
        @endif
    </section>

@endsection

