<?php
use App\Role;
use App\Shift;
use Carbon\Carbon;
$role = new Role();
$shift = new Shift();
?>

@extends('layouts.dashboard')

@section('title','График | Дэшборд')

@section('content')
    <link rel="stylesheet" href="/css/shifts.css">
    <section id="stats">
        <div class="container">
            <div class="accordion" data-token="{{csrf_token()}}"><span>Электричество</span></div>
            <div class="panel shifts-electricity">

                <div class="general">
                    <p>Общее время работы плиты - <? echo $shift->totalHours(); ?> <small>часов</small></p>
                    <p>Затраты на электричество за все время (плита) - <? echo $shift->totalHours() * 5.25 * 0.7; ?> <small>₴</small></p>
                    <small>Потребляемая мощность - 0.7кВт</small>

                    <p>Затраты на электричество за все время (холодильник) - <? echo $shift->daysSinceOpening() * 24 * 5.25 * 0.132; ?> <small>₴</small></p>
                    <small>Потребляемая мощность - 0.132кВт</small>
                    <br>
                    <small>Цена за кВт - <b>5.25 ₴</b></small>
                    <hr>
                    Итого: <? echo ($shift->totalHours() * 5.25 * 0.7) + ($shift->daysSinceOpening() * 24 * 5.25 * 0.132); ?> ₴
                    <br><br>
                </div>

                <div class="custom-period">
                    <h2>Расчет за произвольный период</h2>
                    <div>
                        {{ csrf_field() }}
                        <div class="input-field">
                            <label for="date_start">Дата начала:</label>
                            <input id="date_start" name="date_start" type="date">
                        </div>
                        <div class="input-field">
                            <label for="date_finish">Дата конца:</label>
                            <input id="date_finish" name="date_finish" type="date">
                        </div>
                        <div class="input-field">
                            <label for="price">Стоимость кВт:</label>
                            <input id="price" value="5.25" name="price" type="number">
                        </div>
                        <div class="input-field">
                            <label for="power_freezer">Мощность плиты:</label>
                            <input id="power_freezer" value="0.7" name="power_freezer" type="number">
                        </div>
                        <div class="input-field">
                            <label for="power_ref">Мощность холодильника:</label>
                            <input id="power_ref" value="0.132" name="power_ref" type="number">
                        </div>
                        <button id="customElectricity" value="Рассчитать">Submit</button>
                    </div>
                    <div class="output">
                        <p id="price"></p>
                        <p id="hours"></p>
                    </div>
                    <script>

                        document.getElementById('date_finish').valueAsDate = new Date();
                        document.getElementById('date_start').valueAsDate = new Date(new Date().setDate(new Date().getDate() - 30));

                    </script>
                </div>

                <script>

                    function customElectricity() {
                        console.log(
                            document.getElementById('date_start').value,
                            document.getElementById('date_finish').value,
                            document.getElementById('price').value);
                        event.preventDefault();
                        $.ajaxSetup({
                            headers: {
                                'Content-Type':'application/json',
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            dataType: 'json',
                            type: "get",
                            url: '/shifts/customElectricity',
                            data: ({
                                fromDate: document.getElementById('date_start').value,
                                toDate: document.getElementById('date_finish').value,
                                price: document.getElementById('price').value,
                                powerFreezer: document.getElementById('power_freezer').value,
                                powerRef: document.getElementById('power_ref').value,
                                _method: 'post'
                            }),
                            success: function( response ) {

                                console.log(response);

                                var output = $('.output');
                                output.empty();
//                                var outputHours = $('#hours').empty();

//                                outputPrice.text(response.totalValue);
//                                outputHours.text(response.hours);

                                output.append('<p>Время работы плиты: '+response.activehours+'<small> часов</small></p>');
                                output.append('<p>Затраты на работу плиты: '+response.activevalue+'<small> ₴</small></p>');
                                output.append('<p>Время работы холодильника: '+response.passivehours+'<small> часов</small></p>');
                                output.append('<p>Затраты на работу холодильника: '+response.passivevalue+'<small> ₴</small></p>');
                                output.append('<p>Общие затраты: <b>'+response.totalvalue+'</b><small> ₴</small></p>');

                                output.append('<span> Формула: плита (' + response.activehours + ' * ' + document.getElementById('power_freezer').value + ' * ' + document.getElementById('price').value + ')' + ' + холодильник (' + response.passivehours + ' * '+ document.getElementById('power_ref').value + ' * ' + document.getElementById('price').value + ')</span>');



                                $('.shifts-electricity').css('maxHeight','1000px');


                            }
                        });
                    }

                    $('#customElectricity').on('click', customElectricity);


                </script>
            </div>
           </div>
    </section>
    <section id="shifts">
        <div class="container">
            <div class="shifts table">
                <h1>График работы</h1>
                <div class="table-heading-black">
                    <div class="table-section-small">#</div>
                    <div class="table-section">Дата</div>
                    <div class="table-section-big">Сотрудник</div>
                    <div class="table-section-big">Задачи</div>
                    <div class="table-section">Начало дня</div>
                    <div class="table-section">Конец лня</div>
                    @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                        <div class="table-section-small">
                            <i class="fas fa-edit"></i>
                        </div>
                    @endif

                </div>
                <ul class="table-list">
                    @foreach($shifts as $shift)

                        <li class="table-list-item">

                            <div class="table-section-small">{{ $shift->id or '?' }}</div>
                            <div class="table-section">{{ $shift->date or '?' }}</div>
                            <div class="table-section-big">{{ $shift->staff->name.' '.$shift->staff->surname }}</div>
                            <div class="table-section-big">{{ $shift->description }}</div>
                            <div class="table-section">{{ $shift->starting_at or '?' }}</div>
                            <div class="table-section">{{ $shift->finishing_at or '?' }}</div>

                            @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                                <button class="table-section-small table-edit table-edit-shifts"
                                        id="{{$shift->id}}"
                                        onclick="togglePopupEdit();">
                                    <i class="fas fa-edit"></i>
                                </button>
                            @endif

                        </li>

                    @endforeach
                </ul>
                {!! $shifts->render() !!}
                @if($role->isAdmin()) {{-- ADMIN ONLY --}}
                    <button class="table-add-block" onclick="togglePopupAdd();">
                        <i class="fas fa-plus"></i>
                        <span>Добавить</span>
                    </button>
                    @include('popup.popup-shift-edit')
                    @include('popup.popup-shift-add')
                @endif

            </div>
        </div>
        @if(!$role->isStaff())
            <button id="button-add-general" onclick="togglePopupAdd();">+</button>
        @endif
    </section>

    <script>
        $(document).ready(function(){

            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }

        });
    </script>
@endsection

