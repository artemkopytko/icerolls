<?php
use App\Payment;
?>
<div id="popupAdd" class="popup-payments">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupAdd();"></i>
        </div>
        <h1 id="popup-heading">Добавление платежа</h1>
<?php
	    $paymentTypes = Payment::$types;
//	    var_dump($paymentTypes);
	    ?>
        <form class="popup-form" action="/payments" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="type-add">
                    <span class="required" title="Required field">
                        Вид:
                    </span>
                </label>
                <select name="type" id="type-add" required>
                    @foreach($paymentTypes as $paymentType)
                    <option value="{{$paymentType}}">{{$paymentType}}</option>
                    @endforeach
                    {{--<option value="Единый Налог">Единый Налог</option>--}}
                    {{--<option value="Единый Социальный Взнос">Единый Социальный Взнос</option>--}}
                    {{--<option value="Зарплата">Зарплата</option>--}}
                    {{--<option value="Обслуживание оборудования">Обслуживание оборудования</option>--}}
                    {{--<option value="Закупки">Закупки</option>--}}
                    {{--<option value="Прочее">Прочее</option>--}}
                </select>
            </div>
            <div class="popup-form-row">
                <label for="amount-add">
                    <span class="required" title="Required field">
                        Сумма:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999"
                name="amount" id="amount-add" placeholder="500.0" required>
            </div>
            <div class="popup-form-row">
                <label for="note-add">
                    <span>
                        Комментарий:
                    </span>
                </label>
                <input type="text" name="note" id="note-add">

            </div>
            <div class="popup-form-row">
                <label for="date-add">
                    <span class="required" title="Required field">
                        Дата:
                    </span>
                </label>
                <input type="date" name="date" id="date-add" required>

            </div>
            <div class="popup-form-row">
                <label for="valid_till-add">
                    <span>
                        Действительно до:
                    </span>
                </label>
                <input type="date" name="valid_till" id="valid_till-add">
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name == 'Стафф') disabled @endif>
                    Добавить
                </button>
            </div>
        </form>
    </div>
</div>

<script>
    document.getElementById('date-add').valueAsDate = new Date();
</script>

