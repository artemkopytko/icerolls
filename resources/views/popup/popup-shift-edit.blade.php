<div id="popupEdit" class="popup-shifts">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupEdit();"></i>
        </div>
        <h1 id="popup-heading-edit">Редактирование платежа</h1>
        <form class="popup-form" action="/shifts/$shift->id/edit" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="staff_id-edit">
                    <span class="required" title="Required field">
                        Сотрудник:
                    </span>
                </label>
                <select name="staff_id" id="staff_id-edit" required>
                    @if($staff)
                        @foreach($staff as $st)
                            @if($st->user->activated)
                            <option id="staff_fullname-edit" value="{{$st->id}}">
                                {{ $st->name.' '.$st->surname }}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="popup-form-row">
                <label for="date-edit">
                    <span class="required" title="Required field">
                        Дата:
                    </span>
                </label>
                <input type="date" name="date" id="date-edit">
            </div>
            <div class="popup-form-row">
                <label for="description-edit">
                    <span>
                        Задачи:
                    </span>
                </label>
                <textarea type="text" name="description" id="description-edit"></textarea>
            </div>
            <div class="popup-form-row">
                <label for="starting_at-edit">
                    <span>
                        Начало дня:
                    </span>
                </label>
                <input type="time" name="starting_at" id="starting_at-edit">
            </div>
            <div class="popup-form-row">
                <label for="finishing_at-edit">
                    <span>
                        Конец дня:
                    </span>
                </label>
                <input type="time" name="finishing_at" id="finishing_at-edit">
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Редактировать
                </button>
            </div>
        </form>
    </div>
</div>



