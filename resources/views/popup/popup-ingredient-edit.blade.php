<div id="popupEdit" class="popup-ingredients">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupEdit();"></i>
        </div>
        <h1 id="popup-heading-edit">Редактирование ингредиента</h1>
        <form class="popup-form" action="/payments/$ingredient->id/edit" method="post">
            {{ csrf_field() }}

            <div class="popup-form-row">
                <label for="price-edit">
                    <span class="required" title="Required field">
                        Стоимость:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999" name="price"
                       id="price-edit">
            </div>
            <div class="popup-form-row">
                <label for="title-edit">
                    <span class="required" title="Required field">
                        Название:
                    </span>
                </label>
                <input type="text" name="title" id="title-edit">
            </div>
            <div class="popup-form-row">
                <label for="description-edit">Описание: </label>
                <textarea type="text" name="description"
                          id="description-edit">
                    {{ $ingredient->description or '' }}
                </textarea>
            </div>
            <div class="popup-form-row">
                <label for="amountperpack-edit">
                    <span class="required" title="Required field">
                        Ед. в упаковке:
                    </span>
                </label>
                <input type="number" step="1" max="99999" min="1"
                       name="amountperpack" id="amountperpack-edit" required>
            </div>
            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Редактировать
                </button>
            </div>
        </form>
    </div>
</div>

