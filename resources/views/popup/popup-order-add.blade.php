<div id="popupAdd" class="popup-orders">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupAdd();"></i>
        </div>
        <h1 id="popup-heading">Добавление заказа</h1>
        <form class="popup-form" action="/orders" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="product-add">
                    <span class="required" title="Required field">
                        Продукт:
                    </span>
                </label>
                <select class="product-name" name="product_id[]" id="product-add" required>
                    @foreach($products as $product)
                        <option class="product-option" data-value="{{$product->price}}" value="{{$product->id}}">{{ $product->title.' ('.$product->price.'₴)' }} </option>
                        @endforeach
                </select>
            </div>
            <div class="popup-form-row">
                <label for="quantity-add">
                    <span class="required" title="Required field">
                        Количество:
                    </span>
                </label>
                <select class="product-quantity" name="quantity[]" id="quantity-add">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                </select>
            </div>
            <hr style="width: 100%">
            <div class="extra-products">

            </div>
            <div class="popup-form-button">
                <p class="extra-product-button">Еще продукт</p>
            </div>

            <div class="popup-form-row">
                <p>К оплате: </p><p class="order-sum">Order sum</p><p>₴</p>
            </div>

            <div class="popup-form-button green-button">
                <button>Добавить заказ</button>
            </div>
        </form>


    </div>
</div>

<script>

    var sumField = $('.order-sum');
    var values = [];
    var quantity = [];
    var sum = 0.0;

    function refreshOrder() {
        values = [];
        quantity = [];
        sum = 0.0;
        $('.product-name option:selected').each(function () {
//            console.log(this);
            values.push($(this).data('value'));
        });

        $('.product-quantity option:selected').each(function () {
//            console.log(this);
            quantity.push(this.innerHTML);
        });
//        console.log(values,quantity);
        for(var i = 0; i < values.length; i += 1)
        {
            sum += values[i] * quantity[i];
        }
//        console.log(sum);
        sumField.text(sum);
    }

    function setHandlers() {
        $('.product-quantity').each(function () {
//            console.log($(this));
            $(this).change(function () {
                refreshOrder();
//                console.log('changed');
            })
        });


        $('.product-name').each(function () {
//            console.log($(this));
            $(this).change(function () {
                refreshOrder();
//                console.log('changed');
            })
        });


    }

    $('.extra-product-button').on('click', function () {
        $('.extra-products').append(
        '<div class="extra-product"><div class="popup-form-row"><label for="product-add"><span class="required" title="Required field"> Продукт: </span> </label> <select class="product-name" name="product_id[]" id="product-add" required> @foreach($products as $product) <option class="product-option" data-value="{{$product->price}}"  value="{{$product->id}}">{{ $product->title." (".$product->price."₴)" }} </option> @endforeach </select> </div> <div class="popup-form-row"> <label for="quantity-add"> <span class="required" title="Required field"> Количество: </span> </label> <select  class="product-quantity" name="quantity[]" id="quantity-add"> <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> </select> </div> <hr style="width: 100%"> </div>'
        );
        setHandlers();
        refreshOrder();
    });

    $( document ).ready(function() {
        setHandlers();
        refreshOrder();
    });


</script>

