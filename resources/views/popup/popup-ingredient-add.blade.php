<div id="popupAdd" class="popup-ingredients">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupAdd();"></i>
        </div>
        <h1 id="popup-heading">Добавление ингредиента</h1>
        <form class="popup-form" action="/ingredients" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="price-add">
                    <span class="required" title="Required field">
                        Стоимость:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999" name="price"
                       id="price-add" placeholder="10.0" required>
            </div>
            <div class="popup-form-row">
                <label for="title-add">
                    <span class="required" title="Required field">
                        Название:
                    </span>
                </label>
                <input type="text" name="title" id="title-add"
                       placeholder="Название ингредиента" required>
            </div>
            <div class="popup-form-row">
                <label for="amountperpack-add">
                    <span class="required" title="Required field">
                        Ед. в упаковке:
                    </span>
                </label>
                <input type="number" step="1" max="99999" min="1"
                       name="amountperpack" id="amountperpack-add"
                       placeholder="1" required>
            </div>
            <div class="popup-form-row">
                <label for="description-add">
                    <span>
                        Описание:
                    </span>
                </label>
                <input type="text" name="description" id="description-add">
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Добавить
                </button>
            </div>
        </form>
    </div>
</div>


