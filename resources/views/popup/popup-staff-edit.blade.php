<div id="popupEdit">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupEdit();"></i>
        </div>
        <h1>Редактирование профиля</h1>
        <form class="popup-form" action="/profile/{{$s->id}}/edit" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="name-edit">
                    <span class="required" title="Required field">
                        Имя:
                    </span>
                </label>
                <input type="text" name="name" id="name-edit" required>
            </div>
            <div class="popup-form-row">
                <label for="surname-edit">
                    <span class="required" title="Required field">
                        Фамилия:
                    </span>
                </label>
                <input type="text" name="surname" id="surname-edit" required>
            </div>
            <div class="popup-form-row">
                <label for="address-edit">
                    <span class="required" title="Required field">
                        Адрес:
                    </span>
                </label>
                <input type="text" name="address" id="address-edit" required>
            </div>
            <div class="popup-form-row">
                <label for="phone-edit">
                    <span class="required" title="Required field">
                        Телефон:
                    </span>
                </label>
                <input type="tel" name="phone" id="phone-edit" required>
            </div>
            <div class="popup-form-row">
                <label for="salary-edit">
                    <span class="required" title="Required field">
                        Оклад:
                    </span>
                </label>
                <input type="number" step="1" min="0" max="999999"
                       name="salary" id="salary-edit" required>
            </div>
            <div class="popup-form-row">
                <label for="position-edit">
                    <span class="required" title="Required field">
                        Должность:
                    </span>
                </label>
                <select name="position" id="position-edit" required>
                    <option value="Администратор">Администратор</option>
                    <option value="Бухгалтер">Бухгалтер</option>
                    <option value="Стафф">Стафф</option>
                </select>
            </div>
            <div class="popup-form-row">
                <label for="activated-edit">
                    <span class="required" title="Required field">
                        Статус:
                    </span>
                </label>
                <select name="activated" id="activated-edit" required>
                    <option value="true">Активен</option>
                    <option value="false">Неактивен</option>
                </select>
            </div>
            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Редактировать
                </button>
            </div>
        </form>
    </div>
</div>

