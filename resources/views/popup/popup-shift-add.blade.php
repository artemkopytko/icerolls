<div id="popupAdd" class="popup-shifts">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupAdd();"></i>
        </div>
        <h1 id="popup-heading">Добавление рабочего дня</h1>
        <form class="popup-form" action="/shifts" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="staff_id-edit">
                    <span class="required" title="Required field">
                        Сотрудник:
                    </span>
                </label>
                <select name="staff_id" id="staff_id-edit" required>
                    @foreach($staff as $st)
                        @if($st->user->activated)
                            <option id="staff_fullname-edit" class="staff-fullname" value="{{$st->id}}">{{ $st->name.' '.$st->surname }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="popup-form-row">
                <label for="date-add">
                    <span class="required" title="Required field">
                        Дата:
                    </span>
                </label>
                <input type="date" name="date" id="date-add" required>
            </div>
            <div class="popup-form-row">
                <label for="description-add">
                    <span>
                        Задачи:
                    </span>
                </label>
                <textarea type="text" name="description" id="description-add"></textarea>
            </div>
            <div class="popup-form-row">
                <label for="starting_at-add">
                    <span>
                        Начало дня:
                    </span>
                </label>
                <input type="time" name="starting_at" id="starting_at-add" value="10:00:00">
            </div>
            <div class="popup-form-row">
                <label for="finishing_at-add">
                    <span>
                        Конец дня:
                    </span>
                </label>
                <input type="time" name="finishing_at" id="finishing_at-add" value="20:00:00">
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Добавить
                </button>
            </div>
        </form>
    </div>
</div>

<script>
    document.getElementById('date-add').valueAsDate = new Date();
</script>



