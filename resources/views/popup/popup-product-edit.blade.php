<div id="popupEdit" class="popup-products">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupEdit();"></i>
        </div>
        <h1 id="popup-heading-edit">Редактирование продукта</h1>
        <form class="popup-form" action="/payments/$product->id/edit" method="post">
            {{ csrf_field() }}

            <div class="popup-form-row">
                <label for="price-edit">
                    <span class="required" title="Required field">
                        Стоимость:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999" name="price"
                       id="price-edit">
            </div>
            <div class="popup-form-row">
                <label for="title-edit">
                    <span class="required" title="Required field">
                        Название:
                    </span>
                </label>
                <input type="text" name="title" id="title-edit">
            </div>
            <div class="popup-form-row">
                <label for="description-edit">
                    <span class="required" title="Required field">
                        Описание:
                    </span>
                </label>
                <textarea type="text" name="description"
                          id="description-edit">
                </textarea>
            </div>
            <div class="popup-form-row">
                <label for="actual-edit">
                    <span class="required" title="Required field">
                        Статус:
                    </span>
                </label>
                <select name="actual" id="actual-edit" required>
                    <option value="true">Актуален</option>
                    <option value="false">Архивирован</option>
                </select>
            </div>
            <hr>
            <div class="popup-form-row row-vertical">
                <p>Состав: </p>
                @foreach($ingredients as $ingredient)
                    <div>
                        <input class="ingredient-input" type="checkbox"
                               name="ingredients[]"
                               id="{{ $ingredient->title }}"
                        value="{{ $ingredient->id }}">
                        <label for="{{ $ingredient->title }}">{{ $ingredient->title }}</label>
                    </div>
                @endforeach
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Редактировать
                </button>
            </div>
        </form>
    </div>
</div>

