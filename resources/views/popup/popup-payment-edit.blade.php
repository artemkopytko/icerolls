<?php
use App\Payment;
$paymentTypes = Payment::$types;
?>
<div id="popupEdit" class="popup-payments">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupEdit();"></i>
        </div>
        <h1 id="popup-heading-edit">Редактирование платежа</h1>
        <form class="popup-form" action="/payments/$payment->id/edit" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="type-edit">
                    <span class="required" title="Required field">
                        Вид:
                    </span>
                </label>
                <select name="type" id="type-edit">
                    @foreach($paymentTypes as $paymentType)
                        <option value="{{$paymentType}}">{{$paymentType}}</option>
                    @endforeach
                </select>
            </div>
            <div class="popup-form-row">
                <label for="amount-edit">
                    <span class="required" title="Required field">
                        Сумма:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999" name="amount"
                       id="amount-edit" value="{{$payment->amount or ''}}">
            </div>
            <div class="popup-form-row">
                <label for="note-edit">
                    <span>
                        Комментарий:
                    </span>
                </label>
                <input type="text" name="note" id="note-edit">

            </div>
            <div class="popup-form-row">
                <label for="date-edit">
                    <span class="required" title="Required field">
                        Дата:
                    </span>
                </label>
                <input type="date" name="date" id="date-edit" value="">
            </div>
            <div class="popup-form-row">
                <label for="valid_till-edit">
                    <span>
                        Действительно до:
                    </span>
                </label>
                <input type="date" name="valid_till" id="valid_till-edit">
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name == 'Стафф') disabled @endif>
                    Редактировать
                </button>
            </div>
        </form>
    </div>
</div>

<script>
    document.getElementById('date-edit').valueAsDate = new Date();
</script>

