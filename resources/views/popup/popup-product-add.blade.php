<div id="popupAdd" class="popup-products">
    <div class="popup-content">
        <div class="popup-close">
            <i class="fas fa-times" onclick="togglePopupAdd();"></i>
        </div>
        <h1 id="popup-heading">Добавление продукта</h1>
        <form class="popup-form" action="/products" method="post">
            {{ csrf_field() }}
            <div class="popup-form-row">
                <label for="price-add">
                    <span class="required" title="Required field">
                        Стоимость:
                    </span>
                </label>
                <input type="number" step="0.01" min="0" max="999999" name="price"
                       id="price-add" placeholder="50.0" required>
            </div>
            <div class="popup-form-row">
                <label for="title-add">
                    <span class="required" title="Required field">
                        Название:
                    </span>
                </label>
                <input type="text" name="title" id="title-add"
                       placeholder="Мороженое" required>
            </div>
            <div class="popup-form-row">
                <label for="description-add">
                    <span>
                        Описание:
                    </span>
                </label>
                <textarea type="text" name="description"
                          id="description-add"></textarea>
            </div>
            <hr>
            <div class="popup-form-row row-vertical">
                <p>Состав: </p>
                @foreach($ingredients as $ingredient)
                    <div>
                        <input class="ingredient-input" type="checkbox"
                               name="ingredients[]"
                               id="{{ $ingredient->title }}"
                               value="{{ $ingredient->id }}">
                        <label for="{{ $ingredient->title }}">{{ $ingredient->title }}</label>
                    </div>
                @endforeach
            </div>

            <div class="popup-form-button">
                <button type="submit"
                        @if(Auth::user()->role->display_name !== 'Администратор') disabled @endif>
                    Добавить
                </button>
            </div>
        </form>
    </div>
</div>


