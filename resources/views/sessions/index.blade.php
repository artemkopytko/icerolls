@extends('layouts.master')

@section('content')

    <link rel="stylesheet" href="css/login.css">
    <main class="login">
        <div class="container">
            <div class="login-form">
                <h1 class="main-heading">Авторизация</h1>
                <form action="/login" method="post">
                    {{ csrf_field() }}
                    <div class="input-block">
                        <input class="input-field" id="email" name="email" type="email"
                               title="Email" placeholder="Email" required>
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="input-block">
                        <input class="input-field" id="password" name="password"
                               type="password" title="Password" placeholder="Пароль" required>
                        <i class="fas fa-unlock-alt"></i>
                    </div>
                    <button type="submit">Войти</button>
                    <div class="input-block fdr">
                        <span class="pdr-1">Нет аккаунта? </span><a href="/register"> Зарегистрироваться</a>
                    </div>
                </form>
            </div>
        </div>
    </main>


@endsection