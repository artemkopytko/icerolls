// $( document ).ready(function() {

function taxBook() {
    // var id = this.id;
    var acc = document.getElementsByClassName('taxBookWrapper')[0];
    var token = $('.taxBookWrapper').data('token');
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/payments/taxBook',
        data: {_token: token},
        success: function( response ) {

            showSuccessMessage('Получены последние налоговые платежи');

            response.forEach(function(data){
                $('.taxBook').append(
                    '<div class="small-table-row"><div class="small-table-cell">'+data.date+'</div><div class="small-table-cell">'+data.sum+'</div></div>'
                );
                var panel = acc.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    });
}

function taxBookLastMonth() {
    // var id = this.id;
    var acc = document.getElementsByClassName('taxBookLastMonthWrapper')[0];
    var token = $('.taxBookWrapper').data('token');
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/payments/taxBookLastMonth',
        data: {_token: token},
        success: function( response ) {

            // console.log(response);

            showSuccessMessage('Получены данные книги доходов');

            response.forEach(function(data){


                $('.taxBookLastMonth').append(
                    '<div class="small-table-row"><div class="small-table-cell">'+data.date+'</div><div class="small-table-cell">'+data.sum+'</div></div>'
                );

                var panel = acc.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    });
}

function taxBookCustomPeriodSum() {
    // var id = this.id;
    event.preventDefault();
    var startingAt = $('#customPeriodSumStart').val();
    var finishingAt = $('#customPeriodSumFinish').val();

    var requestData = {
        startingAt: startingAt,
        finishingAt: finishingAt
    }

    var acc = document.getElementsByClassName('taxBookCustomPeriodSumWrapper')[0];
    var token = $('.taxBookWrapper').data('token');
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/payments/taxBookCustomPeriodSum',
        data: requestData,
        success: function( response ) {

            console.log(response);

            showSuccessMessage('Получен подсчет по заданному промежутку времени');

            // response.forEach(function(data){


                // $('.taxBookLastMonth').append(
                //     '<div class="small-table-row"><div class="small-table-cell">'+data.date+'</div><div class="small-table-cell">'+data.sum+'</div></div>'
                // );
$('.taxBookCustomPeriodSum').text('Доход за данный период составляет ' + response.sum + ' грн.');

                var panel = acc.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            // });
        }
    });
}



function taxBookGetMonthlyIncomes() {
    // var id = this.id;
    var acc = document.getElementsByClassName('taxBookGetMonthlyIncomesWrapper')[0];
    var token = $('.taxBookWrapper').data('token');
    $.ajaxSetup({
        headers: {
            'Content-Type':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/payments/taxBookGetMonthlyIncomes',
        data: {_token: token},
        success: function( response ) {

            // console.log(response);

            showSuccessMessage('Получены данные о доходах по месяцам');

            response.forEach(function(data){

                var humanDate = data.date.slice(0, data.date.length-3);

                $('.taxBookGetMonthlyIncomes').append(
                    '<div class="small-table-row"><div class="small-table-cell">'+humanDate+'</div><div class="small-table-cell">'+data.sum+'</div></div>'
                );

                var panel = acc.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    });
}


// });


//# sourceMappingURL=payments.js.map
