
// BASIC SETUP FOR ALL CHARTS

function randomColor()
{
    return '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);

}
var color;

var token = $('#canvas').data('token');
$.ajaxSetup({
    headers: {
        'Content-Type':'application/json',
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// * * * * * * * DETAILED CHARTS * * * * * * *
/*
*
*
* * * * DETAILED WEEK CHART
*
*
*/

function detailedWeekChart()
{
    var thisWeekDays = new Array();

    $.ajax({
        dataType: 'json',
        type: "get",
        async: false,
        url: '/orders/thisWeekDays',
        success: function( response ) {
            response.forEach(function(data){
                thisWeekDays.push(data.day);
            });
            // console.log(thisWeekDays);
        }
    });

    var productNames = new Array();
    $.ajax({
        dataType: 'json',
        type: "get",
        async: false,
        url: '/orders/productNames',
        success: function( response ) {
            response.forEach(function(data){
                productNames.push(data.title);
            });
            // console.log(productNames);
        }
    });

    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/detailedWeekChart',
        success: function( response ) {
            // console.log(response);
            var detailedWeekDays = new Array();
            var detailedWeekSales =new Array();
            var detailedWeekTitles = new Array();
            var rows = response;
            var totalRows = rows.length;
            response.forEach(function(data){

                detailedWeekDays.push(data.day);
                detailedWeekSales.push(data.num);
                detailedWeekTitles.push(data.title);
            });

            var daysAvailable = thisWeekDays.length;
            var productsAvailable = productNames.length;

            var x = new Object();
            var j = 0;
            var i = 0;
            for(i = 0; i<daysAvailable; i++)
            {
                x[i] = new Object();

                for(j = 0; j < productsAvailable; j++)
                {
                    x[i][productNames[j]] = 0;
                }

                for(j = 0; j<totalRows; j++) {
                    if(rows[j].day === thisWeekDays[i]) {
                        x[i][rows[j].title] = rows[j].num;
                    }
                }
            }


            for(i = 0; i<daysAvailable; i++)
            {
                x[i] = new Object();

                for(j = 0; j < productsAvailable; j++)
                {
                    x[i][productNames[j]] = 0;
                }

                for(j = 0; j<totalRows; j++) {
                    if(rows[j].day === thisWeekDays[i]) {
                        x[i][rows[j].title] = rows[j].num;
                    }
                }
            }

            var productStats = new Array(productsAvailable);
            for(i = 0; i < productsAvailable; i++)
            {
                for(j = 0; j<daysAvailable;j++)
                {
                    productStats[i] = new Array(daysAvailable);
                }
                for(j=0;j<daysAvailable;j++) {
                    productStats[i][j] = x[j][productNames[i]];
                    delete productStats[i].data;
                    delete productStats[i]._meta;
                }

            }


            var lineChartData = { labels: thisWeekDays, datasets: []};
            for(i =0; i<productsAvailable;i++)
            {
                color = randomColor();
                lineChartData.datasets.push({
                    label: productNames[i],
                    data: productStats[i],
                    fill: false,
                    borderColor: color,
                    backgroundColor: color
                });
            }


            var ctx = document.getElementById("canvas-detailedWeekChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: lineChartData,
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    });
}

/*
*
*
* * * * DETAILED MONTH CHART
*
*
*/

function detailedMonthChart() {

    var thisMonthDays = new Array();

    $.ajax({
        dataType: 'json',
        type: "get",
        async: false,
        url: '/orders/thisMonthDays',
        success: function( response ) {
            response.forEach(function(data){
                thisMonthDays.push(data.day);
            });
            // console.log(thisMonthDays);
        }
    });

    var productNames = new Array();
    $.ajax({
        dataType: 'json',
        type: "get",
        async: false,
        url: '/orders/productNames',
        success: function( response ) {
            response.forEach(function(data){
                productNames.push(data.title);
            });
            // console.log(productNames);
        }
    });

    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/detailedMonthChart',
        success: function( response ) {
            console.log(response);

            var detailedMonthDays = new Array();
            var detailedMonthSales =new Array();
            var detailedMonthTitles = new Array();
            var rows = response;
            var totalRows = rows.length;
            response.forEach(function(data){

                detailedMonthDays.push(data.day);
                detailedMonthSales.push(data.num);
                detailedMonthTitles.push(data.title);
            });

            var daysAvailable = thisMonthDays.length;
            var productsAvailable = productNames.length;

            var x = new Object();
            var j = 0;
            var i = 0;
            for(i = 0; i<daysAvailable; i++)
            {
                x[i] = new Object();

                for(j = 0; j < productsAvailable; j++)
                {
                    x[i][productNames[j]] = 0;
                }

                for(j = 0; j<totalRows; j++) {
                    if(rows[j].day === thisMonthDays[i]) {
                        x[i][rows[j].title] = rows[j].num;
                    }
                }
            }


            for(i = 0; i<daysAvailable; i++)
            {
                x[i] = new Object();

                for(j = 0; j < productsAvailable; j++)
                {
                    x[i][productNames[j]] = 0;
                }

                for(j = 0; j<totalRows; j++) {
                    if(rows[j].day === thisMonthDays[i]) {
                        x[i][rows[j].title] = rows[j].num;
                    }
                }
            }

            var productStats = new Array(productsAvailable);
            for(i = 0; i < productsAvailable; i++)
            {
                for(j = 0; j<daysAvailable;j++)
                {
                    productStats[i] = new Array(daysAvailable);
                }
                for(j=0;j<daysAvailable;j++) {
                    productStats[i][j] = x[j][productNames[i]];
                    delete productStats[i].data;
                    delete productStats[i]._meta;
                }

            }


            var lineChartData = { labels: thisMonthDays, datasets: []};
            for(i =0; i<productsAvailable;i++)
            {
                color = randomColor();
                lineChartData.datasets.push({
                    label: productNames[i],
                    data: productStats[i],
                    fill: false,
                    borderColor: color,
                    backgroundColor: color
                });
            }


            var ctx = document.getElementById("canvas-detailedMonthChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: lineChartData,
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            // console.log(ctx);
        }
    });
}

// * * * * * * * GENERAL CHARTS * * * * * * *
/*
*
*
* * * * GENERAL WEEK CHART
*
*
*/

function generalWeekChart() {
    $.ajax({
        dataType: 'json',
        type: "get",
        // async: false,
        url: '/orders/generalWeekChart',
        success: function( response ) {
            var lastWeekDays = new Array();
            var lastWeekSales = new Array();

            response.forEach(function(data){
                lastWeekDays.push(data.generalweekchartdays);
                lastWeekSales.push(data.generalweekchartitems);
            });

            var ctx = document.getElementById("canvas-generalWeekChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels:lastWeekDays,
                    datasets: [
                        {
                            label: 'Заказов',
                            data: lastWeekSales,
                            borderWidth: 1,
                            borderColor: randomColor(),
                            backgroundColor: 'transparent'
                        }]
                },
                options: {
                    legend: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    });
}

function generalMonthChart() {
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/generalMonthChart',
        success: function( response ) {
            var lastMonthDays = new Array();
            var lastMonthSales = new Array();

            response.forEach(function(data){
                lastMonthDays.push(data.generalmonthchartdays);
                lastMonthSales.push(data.generalmonthchartitems);
            });

            var ctx = document.getElementById("canvas-generalMonthChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels:lastMonthDays,
                    datasets: [
                        {
                            label: 'Заказов',
                            data: lastMonthSales,
                            borderWidth: 1,
                            borderColor: randomColor(),
                            backgroundColor: 'transparent'
                        }]
                },
                options: {
                    legend: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    });
}


function lastMonthIncomes() {
    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/lastMonthIncomes',
        success: function( response ) {
            var lastMonthDays = new Array();
            var lastMonthSales = new Array();

            response.forEach(function(data){
                lastMonthDays.push(data.date);
                lastMonthSales.push(data.amount);
            });

            var ctx = document.getElementById("canvas-lastMonthIncomes").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels:lastMonthDays,
                    datasets: [
                        {
                            label: 'Заказов',
                            data: lastMonthSales,
                            borderWidth: 1,
                            borderColor: randomColor(),
                            backgroundColor: 'transparent'
                        }]
                },
                options: {
                    legend: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    });
}

/* BARS */
function thisYearMonthSales()
{
    function monthNumberToString(num)
    {
        var monthNo = num.toString();
        console.log(monthNo);
        switch (num)
        {
            case '01':
                return 'Январь';
                break;
            case '02':
                return 'Февраль';
                break;
            case '03':
                return 'Март';
                break;
            case '04':
                return 'Апрель';
                break;
            case '05':
                return 'Май';
                break;
            case '06':
                return 'Июнь';
                break;
            case '07':
                return 'Июль';
                break;
            case '08':
                return 'Август';
                break;
            case '09':
                return 'Сентябрь';
                break;
            case '10':
                return 'Октябрь';
                break;
            case '11':
                return 'Ноябрь';
                break;
            default:
                return 'Декабрь';
                break;
        }
    }
    var thisYearMonths = [];

    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/thisYearMonths',
        success: function( response ) {
            response.forEach(function(data){
                thisYearMonths.push(data.day);
            });
        }
    });

    $.ajax({
        dataType: 'json',
        type: "get",
        url: '/orders/thisYearMonthSales',
        success: function( response ) {
            var thisYearMonthSales = []
            var thisYearMonthLabels = []
            var randomColors = []

            response.forEach(function(data){
                thisYearMonthSales.push(data.sum);
                thisYearMonthLabels.push(data.monno)
            });


            for(var i = 0; i<thisYearMonths.length; i++)
            {
                thisYearMonthLabels[i] = monthNumberToString(thisYearMonthLabels[i]);
                randomColors[i] = randomColor();
            }

            var ctx = document.getElementById("canvas-thisYearMonthSales").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels:thisYearMonthLabels,
                    datasets: [
                        {
                            label: 'Заказы',
                            data: thisYearMonthSales,
                            borderWidth: 1,
                            borderColor: randomColors,
                            // backgroundColor: randomColor(),
                            fill: false,
                            lineTension: 0.1
                        }]
                },
                options: {
                    legend: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });

            var ctx = document.getElementById("canvas-thisYearMonthSalesDoughnut").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels:thisYearMonthLabels,
                    datasets: [
                        {
                            label: 'Заказы',
                            data: thisYearMonthSales,
                            borderWidth: 1,
                            backgroundColor: randomColors,
                            fill: false,
                            lineTension: 0.1
                        }]
                }
            });
        }
    });




}
//# sourceMappingURL=orderCharts.js.map
