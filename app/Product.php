<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $fillable = ['price','amountperpack','title','description','actual'];

	public function ingredients()
	{
		return $this->belongsToMany(Ingredient::class, 'product_ingredient');
	}

	public function orders()
	{
		return $this->hasMany(Order::class, 'product_id');
	}
}
