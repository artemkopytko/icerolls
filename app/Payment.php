<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $timestamps = false;
    protected $fillable = ['type','amount','date','valid_till','note'];

    public static $types = ['Аренда','Единый Налог',
	    'Единый Социальный Взнос','Зарплата', 'Обслуживание Терминала',
	    'Обслуживание Счета', 'Закупка Расходников',
	    'Закупка Ингредиентов', 'Закупка Инструмента',
	    'Обслуживание Оборудования','Закупки', 'Реклама', 'Документы',
	    'Прочее', 'Доставка НП', 'Паркинг', 'Бензин', 'Электричество'];


    public static function balance() {
    	$balance = DB::select('
    	WITH income AS(
		SELECT SUM(amount) as inc 
		from incomes
	), expenses AS (
		SELECT sum(amount) as exp
		FROM Payments)
	SELECT inc - exp AS val from income, expenses
    	');
    	return $balance;
    }

}
