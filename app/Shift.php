<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = ['date','description','starting_at','finishing_at','staff_id'];

    public function staff()
    {
    	return $this->belongsTo(Staff::class, 'staff_id' );
    }

    public static function totalHours() {

    	$totalHours = DB::select('
		WITH totalTime AS (
    	SELECT sum (finishing_at - starting_at) 
		FROM shifts
		)
		SELECT date_part(\'hours\', totalTime.sum)
		FROM totalTime');

    	return $totalHours[0]->date_part;
    }

    public static function daysSinceOpening() {

    	$totalDays = DB::select('
    	WITH FirstDay AS (
		SELECT *
		FROM Shifts
		ORDER BY id asc
		LIMIT 1
		)
		SELECT date_part(\'days\',now()-firstDay.date) from firstDay');

    	return $totalDays[0]->date_part;
    }


}
