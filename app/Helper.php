<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
/**
 * Created by PhpStorm.
 * User: artemkopytko
 * Date: 4/15/19
 * Time: 10:06 PM
 */

class Helper {

	public static function editConfigFile( $fileName, $field, $value ) {

		config([$fileName.'.'.$field => $value]);
		$fp = fopen( base_path() . '/config/' . $fileName . '.php', 'w' );
		$result = fwrite( $fp, '<?php return ' . var_export( config( $fileName ), true ) . ';' );
		fclose( $fp );

		return $result ? 0 : 1;

	}


}