<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
	protected $table = 'staff';
	public $timestamps = false;

	protected $guarded = [''];

	public function user()
	{
		return $this->belongsTo(User::class, 'id');
	}

	public function shiftDays()
	{
		return $this->hasMany(Shift::class, 'staff_id');
	}

	public function orders()
	{
		return $this->hasMany(Order::class, 'staff_id');
	}
}
