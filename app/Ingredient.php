<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
	public $timestamps = false;

	protected $fillable = ['price','title','amountperpack','description'];

    public function products()
    {
    	return $this->belongsToMany(Product::class, 'product_ingredient');
    }
}
