<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
	public $timestamps = false;
	protected $table = 'incomes';
	protected $dates = ['date'];
	protected $fillable = ['note','amount','date','date'];

	public static function totalIncome() {
		$totalIncome =
			DB::SELECT('SELECT 
			SUM(AMOUNT)
			FROM INCOMES');
		return $totalIncome;
	}
}
