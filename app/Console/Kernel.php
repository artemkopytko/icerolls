<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
Use App\Income;
use App\Payment;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
	protected function schedule(Schedule $schedule)
	{
		/*$schedule->call(function () {

		})->everyMinute();*/

		$schedule->call(function () {

			$result = DB::select("SELECT sum(p.price * o.quantity - (p.price * o.quantity * discount / 100))::integer as income, o.date::date
FROM orders AS O JOIN products AS p on (o.product_id = p.id)
WHERE o.date::date = now()::date
group by o.date::date");

			$income = Income::create( [
				'amount' => $result[0]->income,
				'date'   => $result[0]->date,
				'note' => ''
			]);

			$telegramToken = config('variables.settings.Прочее.Токен телеграм бота');
			$telegramChat = config('variables.settings.Прочее.ID чата для бота');

			$dayNo = DB::select('select count(id) as dayno from incomes');
			$balance = Payment::balance();

			$txt = "Рабочий день №{$dayNo[0]->dayno} окончен\nПолучен доход: {$result[0]->income}\nБаланс: {$balance[0]->val}";

//			$testFieldset = "Test: ";
//
//			$arr = array(
//				$testFieldset => 'Test',
//			);
//
//			foreach($arr as $key => $value) {
//				$txt .= "<b>".$key."</b> ".$value."\n";
//			};


			$txt = urlencode($txt);
			$sendToTelegram = fopen("https://api.telegram.org/bot".$telegramToken."/sendMessage?chat_id=".$telegramChat."&parse_mode=html&text={$txt}","r");


		})->dailyAt('23:59');

	}
}
