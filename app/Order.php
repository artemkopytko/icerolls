<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = 'orders';
    protected $dates = ['date'];
    protected $fillable = ['product_id','staff_id','quantity','id','date','order','discount'];

    public function staff()
    {
    	return $this->belongsTo(Staff::class);
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }

	public function scopeFilter($query, $filters)
	{
		if($staff_id = $filters['staff_id'])
		{
			$query->where('staff_id', '=', $staff_id);
		}

	}
}
