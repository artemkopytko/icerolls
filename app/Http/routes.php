<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/shifts/customElectricity', 'ShiftsController@customElectricity');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/settings', 'SettingsController@index');

Route::post('/settings/editconfig', 'SettingsController@editConfig');

Route::get('/profile', 'ProfileController@index')->name('profile');

Route::get('/profile/edit', 'ProfileController@edit')->name('profile-edit');

Route::post('/profile/edit', 'StaffController@update');

Route::post('/profile/{id}/edit', 'StaffController@edit');

Route::get('/staff', 'StaffController@index')->name('staff');

Route::post('/staff/{id}', 'StaffController@show');

Route::post('/staff/{id}/activate', 'StaffController@activate');

Route::delete('/staff/{id}', 'StaffController@destroy');

Route::get('/payments', 'PaymentsController@index')->name('payments');

Route::post('/payments', 'PaymentsController@store');

Route::post('/payments/{id}/edit', 'PaymentsController@edit');

Route::post('/payments/{id}', 'PaymentsController@show');

Route::get('/payments/taxBook', 'PaymentsController@taxBook');

Route::get('/payments/taxBookLastMonth', 'PaymentsController@taxBookLastMonth');

Route::get('/payments/taxBookCustomPeriodSum', 'PaymentsController@taxBookCustomPeriodSum');

Route::get('/payments/taxBookGetMonthlyIncomes', 'PaymentsController@taxBookGetMonthlyIncomes');

Route::delete('/payments/{id}', 'PaymentsController@destroy')->name('delete-payment');

Route::get('/incomes', 'IncomesController@index');

Route::post('/incomes', 'IncomesController@store');

Route::post('/incomes/{id}/edit', 'IncomesController@edit');

Route::post('/incomes/{id}', 'IncomesController@show');

Route::delete('/incomes/{id}', 'IncomessController@destroy')->name('delete-income');


Route::get('/products', 'ProductsController@index')->name('products');

Route::post('/products', 'ProductsController@store');

Route::post('/products/{id}/edit', 'ProductsController@edit');

Route::put('/products/{id}', 'ProductsController@destroy')->name('delete-product');

Route::post('/products/{id}', 'ProductsController@show');

Route::get('/products/getCategories', 'ProductsController@getCategories');

Route::get('/products/getProducts', 'ProductsController@getProducts');


Route::get('/ingredients', 'IngredientsController@index')->name('ingredients');

Route::post('/ingredients', 'IngredientsController@store');

Route::post('/ingredients/{id}/edit', 'IngredientsController@edit');

Route::post('/ingredients/{id}', 'IngredientsController@show');

Route::delete('/ingredients/{id}', 'IngredientsController@destroy')->name('delete-ingredient');

Route::get('/orders', 'OrdersController@index')->name('ordrs');

Route::post('/orders', 'OrdersController@store');

Route::post('/orders/{id}/edit', 'OrdersController@edit');

Route::post('/orders/{id}', 'OrdersController@show');

Route::post('/orders/getOrderPositions/{id}', 'OrdersController@getOrderPositions');

Route::post('/orders/getTodayOrdersForStaff/{id}', 'OrdersController@getTodayOrdersForStaff');

Route::delete('/orders/{id}', 'OrdersController@destroy')->name('delete-order');

Route::get('/orders/generalMonthChart', 'OrdersController@generalMonthChart');

Route::get('/orders/generalWeekChart', 'OrdersController@generalWeekChart');

Route::get('/orders/lastMonthIncomes', 'OrdersController@lastMonthIncomes');

Route::get('/orders/detailedMonthChart', 'OrdersController@detailedMonthChart');

Route::get('/orders/detailedWeekChart', 'OrdersController@detailedWeekChart');

Route::get('/orders/lastMonthDays', 'OrdersController@lastMonthDays');

Route::get('/orders/thisMonthDays', 'OrdersController@thisMonthDays');

Route::get('/orders/thisWeekDays', 'OrdersController@thisWeekDays');

Route::get('/orders/thisYearMonths', 'OrdersController@thisYearMonths');

Route::get('/orders/thisYearMonthSales', 'OrdersController@thisYearMonthSales');

Route::get('/orders/productNames', 'OrdersController@productNames');

Route::post('/orders/api/store', 'OrdersController@apiStore')->name('apiStore');


Route::get('/workplace', 'OrdersController@indexWorkplace')->name('workplace');

Route::get('/shifts', 'ShiftsController@index')->name('shifts');

Route::post('/shifts', 'ShiftsController@store');

Route::post('/shifts/{id}/edit', 'ShiftsController@edit');

Route::post('/shifts/{id}', 'ShiftsController@show');



Route::get('/register', 'RegistrationController@index')->name('register');

Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@index')->name('login');

Route::post('/login', 'SessionsController@store');

Route::get('/logout', 'SessionsController@destroy')->name('logout');





