<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{
	public function __construct() {
		$this->middleware('guest', ['except' => ['index','destroy']]);
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::check()) {
    		return redirect('/dashboard');
	    }
        return view('sessions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

	    $email = request('email');
	    $pwdHash = hash('sha512', request('password'));

	    $user = User::where('email', $email)
	                ->where('password', $pwdHash)
	                ->first();

	    if($user)
	    {
	    	Auth::login($user);
	    }
	    else
	    {
		    session()->flash('messageError', 'Неудачная попытка авторизации');

		    return back()->withErrors([
			    'message' => 'Пожалуйста, убедитесь в корректности введенных данных'
		    ]);
	    }


	    if(auth()->user()->activated)
	    {
		    session()->flash('messageSuccess', 'Успешная авторизация');
	    }
	    else
	    {
		    auth()->logout();
		    session()->flash('messageError', 'Ваш аккаунт неактивен');
		    return redirect()->route('home');
	    }

	    DB::disconnect('pgsql');

	    $user = DB::connection('pgsqlAuth')
	              ->select("
							select users.email, users.password, roles.id, roles.name 
							from users join roles on (users.role_id = roles.id)
							where email = '$email' AND password = '$pwdHash'");

	    if($user)
	    {
		    config(['database.connections.pgsql.username' => $user[0]->name]);
	    	switch ($user[0]->name)
		    {
			    case 'admin':
				    config(['database.connections.pgsql.password' => env('DB_ROLE_ADMIN_PASSWORD')]);
			    	break;
			    case 'accountant':
				    config(['database.connections.pgsql.password' => env('DB_ROLE_ACCOUNTANT_PASSWORD')]);
			    	break;
			    case 'staff':
				    config(['database.connections.pgsql.password' => env('DB_ROLE_STAFF_PASSWORD')]);
			    	break;
			    default:
			    	break;
		    }
	    }

	    DB::reconnect('pgsql');

	    return redirect('/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
	    auth()->logout();

	    return redirect()->home();
    }
}
