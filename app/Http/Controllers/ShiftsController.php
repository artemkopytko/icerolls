<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Shift;
use Illuminate\Http\Request;
use App\Role;
use App\Staff;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class ShiftsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

	    $perPage = 20;

	    $shifts = Shift::orderBy('date','desc')->paginate($perPage);
	    $staff = Staff::all();

	    return view('dashboard.shifts', compact('shifts','staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $this->validate(request(), [
		    'date' => 'required|date',
		    'staff_id' => 'required'
	    ]);

	    $shift = new Shift();

	    $shift->date = request('date');
	    $shift->staff_id = request('staff_id');

	    if($r_desc = request('description')){
		    $shift->description = $r_desc;
	    }
	    if($r_starting_at = request('starting_at')) {
	    	$shift->starting_at = $r_starting_at;
	    }
	    if($r_finishing_at = request('finishing_at')) {
	    	$shift->finishing_at = $r_finishing_at;
	    }

	    $shift->save();

	    session()->flash(
		    'messageSuccess', 'День #'.Shift::orderBy('id','desc')->limit('1')->pluck('id').' успешно добавлен'
	    );

	    return redirect()->route('shifts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $shift = Shift::findOrFail($id);

	    if(!$shift)
	    {
		    $response = [
			    'status' => '404'
		    ];
		    return response()->json($response);
	    } else {

		    $staff_fullname = $shift->staff->name . ' ' . $shift->staff->surname;

		    $response = [
			    'id'             => $shift->id,
			    'date'           => $shift->date,
			    'starting_at'    => $shift->starting_at,
			    'finishing_at'   => $shift->finishing_at,
			    'description'    => $shift->description,
			    'staff_fullname' => $staff_fullname,
			    'staff_id'       => $shift->staff_id,
			    'status'         => '200'
		    ];

		    return response()->json( $response );
	    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $shift = Shift::findOrFail($id);

		if(!$shift)
		{
			return back()->withErrors([
				'message' => 'Shift not found'
			]);
		}

	    $this->validate(request(), [
		    'date' => 'required|date',
		    'staff_id' => 'required'
	    ]);


	    $shift->date = request('date');
	    $shift->staff_id = request('staff_id');

	    if($r_desc = request('description')){
		    $shift->description = $r_desc;
	    }
	    if($r_starting_at = request('starting_at')) {
		    $shift->starting_at = $r_starting_at;
	    }
	    if($r_finishing_at = request('finishing_at')) {
		    $shift->finishing_at = $r_finishing_at;
	    }


	    $shift->save();

	    session()->flash(
		    'messageSuccess', 'День #'.$shift->id.' успешно изменен'
	    );

	    return redirect()->route('shifts');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function customElectricity(Request $request)
    {
    	$fromDate = Input::get("fromDate");
//    	$fromDate = request('fromDate');
    	$toDate = Input::get('toDate');
    	$price = Input::get('price');
    	$powerFreezer = Input::get('powerFreezer');
    	$powerRef = Input::get('powerRef');



	    $data = DB::select("
		SELECT * FROM customElectricity(?, ?, ?, ?, ?)", [$fromDate, $toDate, $price, $powerFreezer, $powerRef]);


//	    $response = [
//		    'fromDate' => $fromDate,
//		    'toDate' => $toDate,
//		    'price' => $price
//	    ];
//	    $response = DB::select("
//    	SELECT * FROM taxBook();");
//
//	    return response()->json($response);
	    return response()->json($data[0]);
    }

}
