<?php

namespace App\Http\Controllers;
use App\Role;
use App\Product;
use App\Ingredient;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{

		$perPage = 20;

		$products = DB::select('select * from GetProductsInfo()');
		$product_ingredient = DB::select('select * from Product_Ingredient()');
		$ingredients = Ingredient::orderBy('title','asc')->get();


		return view('dashboard.products', compact('products', 'product_ingredient', 'ingredients'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $this->validate(request(), [
		    'price' => 'required|min:0|numeric',
		    'title' => 'required'
	    ]);

	    if(request('description')){
		    $product = Product::create( [
			    'price'   => request( 'price' ),
			    'title' => request( 'title' ),
			    'description'   => request( 'description' )
		    ]);
	    } else {
		    $product = Product::create( [
			    'price'   => request( 'price' ),
			    'title' => request( 'title' )
		    ] );
	    }

	    $product->ingredients()->sync($request->ingredients);

	    session()->flash(
		    'messageSuccess', 'Продукт #'.$product->id.' успешно добавлен'
	    );

	    return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $product = Product::findOrFail($id);

	    if($product) {
		    $response = [
			    'id'          => $product->id,
			    'price'       => $product->price,
			    'title'       => $product->title,
			    'description' => $product->description,
			    'status'      => '200',
			    'ingredients' => $product->ingredients->pluck('title'),
			    'actual'      => $product->actual
		    ];
	    } else {
		    $response = [
			    'status'      => '404'
		    ];
	    }
	    return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {


	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $product = Product::findOrFail($id);

	    if(!$product)
	    {
		    return back()->withErrors([
			    'message' => 'Product not found'
		    ]);
	    }

	    $this->validate(request(), [
		    'price' => 'required|min:0|numeric',
		    'title' => 'required',
		    'actual'=> 'required'
	    ]);

	    $product->price = request('price');
	    $product->title = request('title');

	    if($r_description = request('description'))
	    {
		    $product->description = $r_description;
	    }


//	    $staff->user->activated = request('activated');
	    $product->actual = request('actual');

	    $product->ingredients()->sync($request->ingredients);

	    $product->save();

	    session()->flash(
		    'messageSuccess', 'Продукт #'.$product->id.' успешно изменен'
	    );

	    return redirect()->route('products');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

	    if ( Role::isStaff() ) {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors( [
			    'message' => 'Non enough privileges'
		    ] );
	    }

	    $product = Product::findOrFail( $id );

	    if ( $product ) {

	    	//DELETE * FROM PRODUCT_INGREDIENT
			//WHERE ID = $ID

//		    DELETE $PRODUCT

		    session()->flash(
			    'messageSuccess', 'Продукт #' . $id . ' успешно архивирован'
		    );

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];
	    } else
	    {
		    session()->flash(
			    'messageError', 'Продукт #' . $id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);

    }

    public function getCategories() {
		$categories = DB::select('SELECT DISTINCT category FROM products');
		if($categories) {
			$response = [
				'categories'    => $categories,
				'status'        => '400'
			];
		} else {
			$response = [
				'status'        => '200'
			];
		}

	    return response()->json($response);
    }

	public function getProducts() {
		$products = Product::where('actual','=','true')->get();
		if($products) {
			$response = [
				'products'    => $products,
				'status'        => '400'
			];
		} else {
			$response = [
				'status'        => '200'
			];
		}

		return response()->json($response);
	}

}
