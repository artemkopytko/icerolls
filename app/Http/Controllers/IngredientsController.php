<?php

namespace App\Http\Controllers;
use App\Ingredient;
use Illuminate\Http\Request;
use App\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class IngredientsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

	    $perPage = 20;

	    $ingredients = Ingredient::orderBy('id')->paginate($perPage);

	    return view('dashboard.ingredients', compact('ingredients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $this->validate(request(), [
		    'price' => 'required|min:0|numeric',
		    'title' => 'required',
		    'amountperpack' => 'required|min:0|numeric'
	    ]);

	    if(request('description')){
		    $ingredient = Ingredient::create( [
			    'price'   => request( 'price' ),
			    'title' => request( 'title' ),
			    'amountperpack' => request('amountperpack'),
			    'description'   => request( 'description' )
		    ]);
	    } else {
		    $ingredient = Ingredient::create( [
			    'price'   => request( 'price' ),
			    'amountperpack' => request('amountperpack'),
			    'title' => request( 'title' )
		    ] );
	    }

	    session()->flash(
		    'messageSuccess', 'Ингредиент #'.$ingredient->id.' успешно добавлен'
	    );

	    return redirect()->route('ingredients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $ingredient = Ingredient::findOrFail($id);

	    if($ingredient) {
		    $response = [
			    'id'            => $ingredient->id,
			    'amountperpack' => $ingredient->amountperpack,
			    'price'         => $ingredient->price,
			    'title'         => $ingredient->title,
			    'description'   => $ingredient->description,
			    'status'        => '200'
		    ];
	    } else {
	    	$response = [
	    		'status' => '404'
		    ];
	    }

	    return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $ingredient = Ingredient::findOrFail($id);

	    $this->validate(request(), [
		    'price' => 'required|numeric|min:0',
		    'title' => 'required',
		    'amountperpack' => 'required|min:0|numeric'
	    ]);

	    $ingredient->price = request('price');
	    $ingredient->title = request('title');
	    $ingredient->amountperpack = request('amountperpack');

	    if($r_description = request('description'))
	    {
		    $ingredient->description = $r_description;
	    }

	    $ingredient->save();

	    session()->flash(
		    'messageSuccess', 'Ингредиент #'.$ingredient->id.' успешно изменен'
	    );

	    return redirect()->route('ingredients');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    if ( Role::isStaff() ) {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors( [
			    'message' => 'Non enough privileges'
		    ] );
	    }

	    $ingredient = Ingredient::findOrFail( $id );

//	    dd($ingredient);

	    if ( $ingredient ) {

		    $ingredient->delete();

		    session()->flash(
			    'messageSuccess', 'Ингредиент #' . $ingredient->id . ' успешно удален'
		    );

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];
	    } else
	    {
		    session()->flash(
			    'messageError', 'Ингредиент #' . $ingredient->id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);
    }
}
