<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Order;
use App\Role;
use App\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use function Sodium\add;

class OrdersController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth',['except' => array('apiStore')]);
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


	    $perPage = 20;

	    if(Role::isStaff()) {
		    $orders = Order::distinct()
		                   ->select('id')
		                   ->orderBy( 'id', 'desc' )
		                   ->filter( [
			                   'staff_id' => auth()->user()->id
		                   ] )
		                   ->paginate( $perPage );
	    } else {
		    $orders = Order::distinct()
						    ->select('id')
						    ->orderBy( 'id', 'desc' )
		                    ->paginate( $perPage );
	    }

	    $products = Product::where('actual','=','true')->get();

	    $allTimeStats = DB::select("
		SELECT * FROM allTimeStats();");

	    $monthStats = DB::select("
	    SELECT * FROM monthStats();");

	    $weekStats = DB::select("
	    SELECT * FROM weekStats();");

	    return view('dashboard.orders',
		    compact('orders', 'products',
			    'allTimeStats','monthStats',
			    'weekStats'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	   // Validation
//	    TODO : VALIDATE ORDER ADD

    	$i = 0;
    	$elements = count($request->product_id);

//    	$product_ids = $request->product_id;
//    	$product_quants = $request->quantity;
	    $orderId = 0;
	    $createdProducts = [];

    	for($i; $i<$elements; $i += 1)
	    {
	    	if($orderId === 0) {
			    Order::create(
				    [
					    'product_id' => $request->product_id[ $i ],
					    'quantity'   => $request->quantity[ $i ],
					    'staff_id'   => auth()->id(),
					    'date'       => Carbon::now('Europe/Kiev')
				    ]);
			    array_push($createdProducts, $request->product_id[$i]);
			    $orderId = Order::all()->last()->id;
		    } else {
	    		//Если есть еще один продукт
	    		// Если продукт уже создан в этой транзакции
//			    То найти его, и увеличить количесвто
	    		if(in_array($request->product_id[$i], $createdProducts)) {
				    $alreadyCreatedOrder = Order::orderBy('id','desc')->where('product_id','=',$request->product_id[$i])->first();
				    $alreadyCreatedOrder->quantity += $request->quantity[$i];
				    $alreadyCreatedOrder->save();
			    }
//			    В противном случае добавить новый
			    else {
				    Order::create(
					    [
						    'id'         => $orderId,
						    'product_id' => $request->product_id[ $i ],
						    'quantity'   => $request->quantity[ $i ],
						    'staff_id'   => auth()->id(),
						    'date'       => Carbon::now( 'Europe/Kiev' )
					    ] );
			    }
		    }
	    }

	    session()->flash(
		    'messageSuccess', 'Заказ #'.$orderId.' успешно добавлен'
	    );

	    return redirect('/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


	    $order = Order::findOrFail( $id );

	    if ( $order ) {

		    if ( Role::isStaff() ) {
			    if($order->staff_id != auth()->user()->id)
			    {
				    session()->flash(
					    'messageError', 'Нельзя удалить чужой заказ'
				    );
				    return redirect('/orders');
			    }
		    }

		    $order->delete();

		    session()->flash(
			    'messageSuccess', 'Заказ #' . $id . ' успешно удален'
		    );

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];
	    } else
	    {
		    session()->flash(
			    'messageError', 'Заказ #' . $id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);
    }


    public function generalMonthChart()
    {
	    $response = DB::select("
	    
	    WITH data as (
		SELECT count(id) as generalMonthChartItems, date::date as generalMonthChartDays
	    FROM Orders
	    WHERE date > now() - interval '1 month'
		GROUP BY 2
		order by 2
			)
			SELECT data.generalMonthChartItems as generalMonthChartItems, to_char(data.generalMonthChartDays, 'DD-MM') as generalMonthChartDays
			FROM data
	    ");
//	    echo $response;
	    return response()->json($response);

    }

	public function generalWeekChart()
	{
		$response = DB::select("
		 WITH data as (
		SELECT count(id) as generalWeekChartItems, date::date as generalWeekChartDays
	    FROM Orders
	    WHERE date > now() - interval '1 week'
		GROUP BY 2
		order by 2
			)
			SELECT data.generalWeekChartItems as generalWeekChartItems, to_char(data.generalWeekChartDays, 'DD-MM') as generalWeekChartDays
			FROM data
	    ");
		return response()->json($response);

	}

	public function lastMonthIncomes()
	{
		$response = DB::select("
		 WITH DATA AS (
	select sum(amount) as amount, date 
	from incomes
	group by date
	order by 2 desc
	limit 30
	)
	select amount, to_char(date,'DD-MM') as date
	from data 
	    ");
		return response()->json($response);

	}

	public function detailedMonthChart()
	{
		$response = DB::select("
	    WITH LastMonthOrders AS 
		(SELECT * 
	    FROM ORDERS
	    WHERE DATE > now() - interval '1 month'
	    )

		SELECT  p.TITLE as title,to_char(o.date,'DD-MM') as day, CASE WHEN sum(o.quantity) <> 0 THEN sum(o.quantity) ELSE 0 END as num
		FROM PRODUCTS as p LEFT JOIN LastMonthOrders as o ON (p.id = o.product_id)
		WHERE p.actual = 't'
		group by 1,2,o.product_id
		order by 2 desc;
	    ");
		return response()->json($response);

	}

	public function detailedWeekChart()
	{
		$response = DB::select("
	    WITH LastWeekOrders AS 
		(SELECT * 
		 FROM ORDERS
		 WHERE DATE >= now() - interval '1 week'
		 )

		SELECT  p.TITLE as title,to_char(o.date,'DD-MM') as day, CASE WHEN sum(o.quantity) <> 0 THEN sum(o.quantity) ELSE 0 END as num
		FROM PRODUCTS as p LEFT JOIN LastWeekOrders as o ON (p.id = o.product_id)
		WHERE p.actual IS  TRUE
		group by 1,2,o.product_id
		order by 2 desc;
	    ");
		return response()->json($response);

	}

	public function lastMonthDays()
	{
		$response = DB::select("
		SELECT DISTINCT to_char(date, 'DD-MM') as lastMonthDays
	    FROM Orders AS O JOIN Products AS P ON (O.product_id = P.id)
	    WHERE date > now() - interval '1 week'
		ORDER BY 1;
		");
		return response()->json($response);
	}

	public function thisMonthDays()
	{
		$response = DB::select("
		SELECT DISTINCT to_char(date, 'DD-MM') as day, to_char(date, 'MM')
	    FROM Orders AS O JOIN Products AS P ON (O.product_id = P.id)
	    WHERE date > now() - interval '1 month'
		ORDER BY 2;
		");
		return response()->json($response);
	}

	public function thisWeekDays()
	{
		$response = DB::select("
		SELECT DISTINCT to_char(date, 'DD-MM') as day
	    FROM Orders AS O JOIN Products AS P ON (O.product_id = P.id)
	    WHERE date > now() - interval '1 week'
		ORDER BY 1;
		");
		return response()->json($response);
	}

	public function productNames()
	{
		$response = DB::select("
		SELECT DISTINCT title
	    FROM Products AS P 
		");
		return response()->json($response);
	}

	public function thisYearMonths()
	{
		$response = DB::select("
		select distinct to_char(date,'mm')
		FROM orders
		WHERE EXTRACT(YEAR FROM date) = EXTRACT(YEAR FROM now())
		order by 1");
		return response()->json($response);
	}

	public function thisYearMonthSales()
	{
		$response = DB::select("
		SELECT distinct sum(quantity) over (partition by to_char(date,'MM')), to_char(date,'MM') as monNo
		FROM Orders AS O
		WHERE EXTRACT(YEAR FROM date) = EXTRACT(YEAR FROM now())
		order by 2
		");
		return response()->json($response);
	}

	public function allTimeStats()
	{

	}

	public function thisYearStats()
	{

	}

	public function thisMonthStats()
	{

	}

	public function thisWeekStats()
	{

	}

    public function indexWorkplace()
    {
    	return view('dashboard.workplace');
    }

    public function apiStore(Request $request) {


    	$response = [];

	    DB::beginTransaction();


//	    DB::disconnect('pgsql');

//	    DB::connect('pgsqlAuth');

//	    TODO: HERE!!!
//	    foreach ($request->all() as $key => $orders) {
//	    	foreach ($orders['order'] as $orderKey => $order) {
//			    $response = [
//				    'message' => $order,
//				    'error'   => [],
//				    'status'  => '200'
//			    ];
//		    }
//	    }
//
//	    return response()->json( $response );

//	    $response = [
//		    'message' => $request->all(),
//		    'len' => sizeof($request->all()),
//		    'error'   => [],
//		    'status'  => '200'
//	    ];
//
//	    return response()->json( $response );


	    try {
//		    $order   = $request->all()['order'];
		    $orderID = 0;

//		    $var = 100 / 0;

//		    foreach ( $order as $key => $orderPosition ) {
		    foreach ($request->all() as $key => $orders) {
			    foreach ($orders['order'] as $orderKey => $orderPosition) {
				    if ( $orderID == 0 ) {
					    $created = Order::create( [
						    'product_id' => $orderPosition['product_id'],
						    'quantity'   => $orderPosition['quantity'],
						    'staff_id'   => auth()->id(),
						    'discount'   => $orderPosition['discount'],
						    'date'       => Carbon::now( 'Europe/Kiev' )
					    ] );
					    $orderID = Order::all()->last()['id'];
				    } else {

					    $created = Order::create( [
						    'id'         => $orderID,
						    'product_id' => $orderPosition['product_id'],
						    'quantity'   => $orderPosition['quantity'],
						    'staff_id'   => auth()->id(),
						    'discount'   => $orderPosition['discount'],
						    'date'       => Carbon::now( 'Europe/Kiev' )
					    ] );

				    }
			    }}
//		    }

		    $response = [
			    'message' => 'Successfully created',
			    'error'   => [],
			    'status'  => '200'
		    ];

		    DB::commit();

		    return response()->json( $response );

	    } catch (\Exception $e) {
	    	DB::rollback();

//		    foreach ( $order as $key => $orderPosition ) {
		    foreach ($request->all() as $key => $orders) {
			    foreach ($orders['order'] as $orderKey => $orderPosition) {
//				    array_push( $response['error'], $orderPosition );
				    $response['status']  = '400';
				    $response['message'] = 'Error';
			    }}
//		    }

		    return response()->json( $response );

	    }
    }

	public function getOrderPositions($orderId, Request $request) {

//    	$response = [
//			'status'  => '200',
//			'id' => $request->id
//		];
//    	return response()->json($response);

    	if($orderPositions = DB::select('select * from getOrdersForHumans() where id = ?', [$orderId])) {
    		$response = [
    			'status'  => '200',
			    'positions' => $orderPositions
		    ];
    		return response()->json($response);
	    } else {
    		$response = [
    			'status' => '400'
		    ];
		    return response()->json($response);
	    }

	}

	public function getTodayOrdersForStaff($staffId, Request $request) {
    	$count = DB::select('SELECT count(DISTINCT o.id) as orders
FROM ORDERS as o WHERE o.staff_id = ? AND o.date::date = now()::date', [$staffId]);
    	if(!$count) {
		    $response = [
			    'status' => '400'
		    ];
		    return response()->json($response);
	    } else {
		    $response = [
			    'status'  => '200',
			    'quantity' => $count
		    ];
		    return response()->json($response);
	    }
	}
}
