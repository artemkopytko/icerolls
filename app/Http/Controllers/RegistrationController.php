<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Staff;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('registration.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $this->validate(request(), [
		    'email' => 'required|email',
		    'password' => 'required|confirmed',
		    'name' => 'required|min:2',
		    'surname' => 'required|min:2',
		    'phone' => 'required|min:10'
	    ]);

	    $email = request('email');
	    $pwdHash = hash('sha512', request('password'));

	    $user = User::create([
		    'email' => $email,
		    'password' => $pwdHash,
		    'role_id' => 3
	    ]);

	    //Running as host
	    config(['database.connections.pgsqlAuth.username' => env('DB_USERNAME')]);
	    config(['database.connections.pgsqlAuth.password' => env('DB_PASSWORD')]);

		// Add a user to the second database
	    DB::connection('pgsqlAuth')
	      ->select("INSERT INTO users(email, password, role_id)
			VALUES 
			('$email','$pwdHash',3)");

	    Staff::create([
	    	'id' => (User::orderBy('id','desc')->get()->first()->id),
		    'position' => $user->role->display_name,
		    'salary' => 0,
		    'name' => request('name'),
		    'surname' => request('surname'),
		    'phone' => request('phone')
		    ]);


		/* Email Welcome
	    Mail::send('emails.signedup', ['user' => $user], function ($m) use ($user) {
		    $m->from('admin@artemkopytko.com', 'Thanks for signing up!');
		    $m->to($user->email)->subject('Welcome');
	    });
		*/

	    if($user->activated)
	    {
		    session()->flash('messageSuccess', 'Вы успешно зарегистрированы');
		    auth()->login($user);
	    } else {
		    session()->flash('messageSuccess', 'Вы успешно зарегистрированы, дождитесь проверки администратором');
		    return redirect()->route('home');
	    }

	    return redirect('/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
