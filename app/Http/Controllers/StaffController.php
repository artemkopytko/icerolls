<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Staff;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}


	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $perPage = 20;

        $staff = Staff::orderBy('id')->paginate($perPage);

        $staffRating = DB::select('select * from Staff_Rating_Income_Orders()');
        $staffUptime = DB::select('select * from Staff_Working_Time()');

        return view('dashboard.staff', compact(['staff','staffRating', 'staffUptime']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $staff = Staff::findOrFail($id);

	    if(!$staff)
	    {

		    $response = [
			    'status' => '404'
		    ];

		    return response()->json($response);

	    } else {

		    $response = [
			    'id'        => $staff->id,
			    'name'      => $staff->name,
			    'surname'   => $staff->surname,
			    'phone'     => $staff->phone,
			    'address'   => $staff->address,
			    'position'  => $staff->position,
			    'salary'    => $staff->salary,
			    'activated' => $staff->user->activated,
			    'status'    => '200'
		    ];

		    return response()->json( $response );
	    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Not enough privileges'
		    ]);
	    }

    	$staff = Staff::findOrFail($id);

		if(!$staff)
		{
			return back()->withErrors([
				'message' => 'Not enough privileges'
			]);
		}

    	$this->validate(request(), [
    		'name'      => 'required',
		    'surname'   => 'required',
		    'address'   => 'required',
		    'phone'     => 'required',
		    'salary'    => 'required|numeric|min:0',
		    'position'  => 'required|in:Администратор,Бухгалтер,Стафф',
		    'activated' => 'required'
	    ]);

    	$requestPosition = request('position');

    	if($requestPosition == 'Администратор')
	    {
	    	$staff->user->role_id = 1;
	    }
	    else if($requestPosition == 'Бухгалтер')
	    {
		    $staff->user->role_id = 2;
	    }
	    else
	    {
		    $staff->user->role_id = 3;
	    }

    	$staff->name = request('name');
    	$staff->surname = request('surname');
    	$staff->address = request('address');
    	$staff->phone = request('phone');
    	$staff->salary = request('salary');
    	$staff->position = request('position');
    	$staff->user->activated = request('activated');

    	$staff->save();
    	$staff->user->save();

    	//EDITING LOGIN ROLE FOR A USER

	    config(['database.connections.pgsqlAuth.username' => env('DB_USERNAME')]);
	    config(['database.connections.pgsqlAuth.password' => env('DB_PASSWORD')]);

	    $userNewRole = $staff->user->role_id;
	    $userEmail = $staff->user->email;

	    DB::connection('pgsqlAuth')
	      ->select("UPDATE users
	      SET role_id = '$userNewRole'
	      WHERE email = '$userEmail';");

	    DB::disconnect('pgsqlAuth');

	    session()->flash(
		    'messageSuccess', 'Профиль #'.$staff->id.' успешно изменен'
	    );

	    return redirect()->route('staff');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    	$staff = Staff::find(auth()->user()->id);

    	if(!$staff)
	    {
		    return back()->withErrors([
			    'message' => 'Staff not found'
		    ]);
	    }

	    $this->validate( request(), [
		    'name'    => 'required|min:2',
		    'surname' => 'required|min:2',
		    'address' => 'required',
		    'phone'   => 'required|min:10'
	    ] );


	    $staff->name = request('name');
	    $staff->surname = request('surname');
	    $staff->address = request('address');
	    $staff->phone = request('phone');

	    $staff->save();

	    session()->flash(
		    'messageSuccess', 'Профиль успешно изменен'
	    );

	    return redirect()->route('profile');

    }

	public function activate($id)
	{
		$staff = Staff::findOrFail($id);

		if(!$staff)
		{
			$response = [
				'status' => '404'
			];
			return response()->json($response);
		}

		$status = '';

		if(!Role::isStaff())
		{
			$staff->user->activated = true;
			$staff->user->save();
			$status = '200';
		} else {
			$status = '403';
		}

		$response = [
			'id' => $id,
			'status' => $status
		];

		return response()->json($response);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    if ( Role::isStaff() ) {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors( [
			    'message' => 'Non enough privileges'
		    ] );
	    }

	    $staff = Staff::findOrFail( $id );

	    if ( $staff ) {

		    $staff->delete();
		    $staff->user->delete();

		    session()->flash(
			    'messageSuccess', 'Сотрудник #' . $id . ' успешно удален'
		    );

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];

	    } else
	    {
		    session()->flash(
			    'messageError', 'Сотрудник #' . $id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);
    }
}
