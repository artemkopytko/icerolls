<?php

namespace App\Http\Controllers;

use App\Income;
use App\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IncomesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $perPage = 20;
	    $incomes = Income::orderBy('date','desc')->paginate($perPage);

	    return view('dashboard.incomes', compact('incomes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $this->validate(request(), [
		    'amount' => 'required|min:0|numeric',
		    'date' => 'required|date',
	    ]);

	    if(request('note')){
		    $income = Income::create( [
			    'amount' => request( 'amount' ),
			    'date'   => request( 'date' ),
			    'note' => request('note')
		    ]);
	    } else {
		    $income = Income::create( [
			    'amount' => request( 'amount' ),
			    'date'   => request( 'date' ),
		    ] );
	    }


	    session()->flash(
		    'messageSuccess', 'Зачисление #'.$income->id.' успешно добавлен'
	    );

	    return redirect('/incomes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $income = Income::findOrFail($id);

	    if($income) {
		    $response = [
			    'id'         => $income->id,
			    'amount'     => $income->amount,
			    'date'       => $income->date,
			    'note'       => $income->note,
			    'status'     => '200'
		    ];
	    }
	    else
	    {
		    $response = [
			    'status'     => '404'
		    ];
	    }

	    return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
	    if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $income = Income::findOrFail($id);

	    if(!$income)
	    {
		    session()->flash(
			    'messageError', 'Доход #'.$income->id.' не найден'
		    );

		    return back()->withErrors([
			    'message' => 'Payment not found'
		    ]);
	    }

	    $this->validate(request(), [
		    'amount' => 'required|min:0|numeric',
		    'date' => 'required|date'
	    ]);

	    $income->amount = request('amount');
	    $income->date = request('date');
	    $income->note = request('note');

	    $income->save();

	    session()->flash(
		    'messageSuccess', 'Платеж #'.$id.' успешно изменен'
	    );

	    return redirect('/incomes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    if ( Role::isStaff() ) {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors( [
			    'message' => 'Non enough privileges'
		    ] );
	    }

	    $income = Income::findOrFail( $id );

	    if ( $income ) {

		    $income->delete();

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];
	    } else
	    {
		    session()->flash(
			    'messageError', 'Доход #' . $id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);
    }
}
