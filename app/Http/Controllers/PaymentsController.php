<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$perPage = 20;
    	$payments = Payment::orderBy('date','desc')->orderBy('id','desc')->paginate($perPage);
    	$latestEN = DB::SELECT("SELECT * FROM latestEN()");
    	$latestESV = DB::SELECT("SELECT * FROM latestESV()");
    	$latestRent = DB::SELECT("SELECT * FROM latestRent()");
    	$thisYearTaxes_ESV = DB::SELECT("SELECT * FROM thisYearTaxes_ESV()");
    	$thisYearTaxes_ESV_total = DB::SELECT("SELECT * FROM thisYearTaxes_ESV_total()");

        return view('dashboard.payments',
	        compact('payments',
		        'latestEN',
		        'latestESV',
		        'latestRent',
		        'thisYearTaxes_ESV',
		        'thisYearTaxes_ESV_total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors([
			    'message' => 'Non enough privileges'
		    ]);
	    }

	    $this->validate(request(), [
		    'type' => 'required',
		    'amount' => 'required|min:0|numeric',
		    'date' => 'required|date',
		    'valid_till' => 'after:date'
	    ]);

    	if(!in_array($request->type,Payment::$types))
	    {
		    session()->flash(
			    'messageError', 'Неверный тип платежа'
		    );

		    return redirect('/payments');
	    }

		if(request('valid_till')){
			$payment = Payment::create( [
				'type'   => request( 'type' ),
				'amount' => request( 'amount' ),
				'date'   => request( 'date' ),
				'valid_till' => request('valid_till'),
				'note' => request('note')
			]);
		} else {
			$payment = Payment::create( [
				'type'   => request( 'type' ),
				'amount' => request( 'amount' ),
				'date'   => request( 'date' ),
				'note' => request('note')
			] );
		}



	    session()->flash(
		    'messageSuccess', 'Платеж #'.$payment->id.' успешно добавлен'
	    );

	    return redirect('/payments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

        $payment = Payment::findOrFail($id);

        if($payment) {
	        $response = [
		        'id'         => $payment->id,
		        'type'       => $payment->type,
		        'amount'     => $payment->amount,
		        'date'       => $payment->date,
		        'valid_till' => $payment->valid_till,
		        'note'       => $payment->note,
		        'status'     => '200'
	        ];
        }
        else
        {
	        $response = [
		        'status'     => '404'
	        ];
        }

        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
    	if(Role::isStaff())
	    {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

	    	return back()->withErrors([
	    		'message' => 'Non enough privileges'
		    ]);
	    }

	    $payment = Payment::findOrFail($id);

    	if(!$payment)
	    {
		    session()->flash(
			    'messageError', 'Платеж #'.$payment->id.' не найден'
		    );

		    return back()->withErrors([
			    'message' => 'Payment not found'
		    ]);
	    }

	    $this->validate(request(), [
		    'type' => 'required',
		    'amount' => 'required|min:0|numeric',
		    'date' => 'required|date',
		    'valid_till' => 'after:date'
	    ]);

	    if(!in_array($request->type,Payment::$types))
	    {
		    session()->flash(
			    'messageError', 'Неверный тип платежа'
		    );

		    return redirect('/payments');
	    }
	    
	    $payment->type = request('type');
	    $payment->amount = request('amount');
	    $payment->date = request('date');
	    $payment->note = request('note');

	    if($r_valid_till = request('valid_till'))
	    {
	    	$payment->valid_till = $r_valid_till;
	    }

	    $payment->save();

	    session()->flash(
		    'messageSuccess', 'Платеж #'.$payment->id.' успешно изменен'
	    );

	    return redirect()->route('payments');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

	    if ( Role::isStaff() ) {
		    session()->flash(
			    'messageError', 'Not enough privileges'
		    );

		    return back()->withErrors( [
			    'message' => 'Non enough privileges'
		    ] );
	    }

	    $payment = Payment::findOrFail( $id );

	    if ( $payment ) {

		    $payment->delete();

//		    session()->flash(
//			    'messageSuccess', 'Платеж #' . $payment->id . ' успешно удален'
//		    );

		    $response = [
			    'id'     => $id,
			    'status' => '200'
		    ];
	    } else
	    	{
		    session()->flash(
			    'messageError', 'Платеж #' . $payment->id . ' не найден'
		    );

		    $response = [
			    'status' => '404'
		    ];
	    }
	    return response()->json($response);
    }

    public function taxBook(Request $request) {
//    	$fromDate = '2018-01-01';
//    	$toDate = 'now()';
//    	if(request()->fromDate)
//		    $fromDate = request()->fromDate;
//    	if(request()->toDate)
//		    $toDate = request()->toDate;

    	$response = DB::select("
    	SELECT * FROM taxBook();");

    	return response()->json($response);
    }

    public function taxBookLastMonth() {
    	$response = DB::select("
    	SELECT amount as sum, date FROM incomes limit 30;");

    	return response()->json($response);
    }

	public function taxBookCustomPeriodSum(Request $request) {
		$result = DB::select("
		SELECT sum(amount) as sum 
FROM incomes 
WHERE date >= ? AND date <= ?;
		", [$request['startingAt'], $request['finishingAt']]);

//		$response = [
//			'sum' => $result,
//			'start' => $request['startingAt'],
//			'finish' => $request['finishingAt']
//		];
//
//		return response()->json($response);

		if($result[0]->sum == null) {
			$response = [
				'sum' => 0
			];
		} else {
			$response = [
				'sum' => $result[0]->sum
			];
		}

		return response()->json($response);
	}

	public function taxBookGetMonthlyIncomes() {
    	$result = DB::select("
    	SELECT sum(amount) as sum, date_trunc('month',date)::date as date
FROM incomes 
GROUP BY date_trunc('month',date)::date;");

		return response()->json($result);
	}



}
